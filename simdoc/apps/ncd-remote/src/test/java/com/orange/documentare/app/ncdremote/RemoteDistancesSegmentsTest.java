package com.orange.documentare.app.ncdremote;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.documentare.app.ncdremote.MatrixDistancesSegments.MatrixDistancesSegment;
import com.orange.documentare.app.ncdremote.request.RequestBuilder;
import com.orange.documentare.app.ncdremote.request.Requests;
import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.model.json.JsonGenericHandler;
import feign.Response;
import org.fest.assertions.Assertions;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;
import java.util.stream.IntStream;

public class RemoteDistancesSegmentsTest {

  private final TestAnimalsElements testAnimalsElements = new TestAnimalsElements();

  @Test
  public void remote_computation_on_same_elements() throws IOException {
    // Given
    ExportModel referenceModel = readReferenceForSameArray();

    BytesData[] elements = testAnimalsElements.elements();
    MatrixDistancesSegments matrixDistancesSegments = new MatrixDistancesSegments(elements, elements);
    matrixDistancesSegments = matrixDistancesSegments.buildSegments();

    RemoteDistancesSegments remoteDistancesSegments =
            new RemoteDistancesSegments(new TestRequestBuilder("map_task_id_results.json"), matrixDistancesSegments);

    // When
    File rHosts = new File(getClass().getResource("/remoteHosts.json").getFile());
    List<MatrixDistancesSegment> segments = remoteDistancesSegments.compute(rHosts);

    // Then
    ExportModel exportModel = new ExportModel(elements, elements, segments);
    Assertions.assertThat(exportModel).isEqualTo(referenceModel);
  }

  @Test
  public void remote_computation_on_distinct_elements_arrays() throws IOException {
    // Given
    ExportModel referenceModel = readReferenceForDistinctArray();

    BytesData[] elements1 = testAnimalsElements.elements();
    BytesData[] elements2 = testAnimalsElements.elements();
    MatrixDistancesSegments matrixDistancesSegments = new MatrixDistancesSegments(elements1, elements2);
    matrixDistancesSegments = matrixDistancesSegments.buildSegments();

    RemoteDistancesSegments remoteDistancesSegments =
            new RemoteDistancesSegments(new TestRequestBuilder("map_task_id_results_distinct_arrays.json"), matrixDistancesSegments);

    // When
    File rHosts = new File(getClass().getResource("/remoteHosts.json").getFile());
    List<MatrixDistancesSegment> segments = remoteDistancesSegments.compute(rHosts);

    // Then
    segments.stream().forEach(segment -> doAssertion(segment, referenceModel));

    ExportModel exportModel = new ExportModel(elements1, elements2, segments);
    Assertions.assertThat(exportModel).isEqualTo(referenceModel);
  }

  private void doAssertion(MatrixDistancesSegment segment, ExportModel model) {
    String id = new File(segment.element.filepaths.get(0)).getName();
    int refIndex = IntStream.range(0, model.items1.length)
            .filter(index -> model.items1[index].relativeFilename.equals(id))
            .findFirst()
            .getAsInt();
    Assertions.assertThat(segment.distances).isEqualTo(model.distancesArray.getDistancesFor(refIndex));
  }

  private ExportModel readReferenceForSameArray() throws IOException {
    return readReference("/animals-dna-same-array-ncd_regular_files_model.json.gz");
  }

  private ExportModel readReferenceForDistinctArray() throws IOException {
    return readReference("/animals-dna-ncd_regular_files_model.json.gz");
  }

  private ExportModel readReference(String resId) throws IOException {
    File file = new File(getClass().getResource(resId).getFile());
    return (ExportModel) (new JsonGenericHandler()).getObjectFromJsonGzipFile(ExportModel.class, file);
  }

  private class TestRequestBuilder implements RequestBuilder {

    private final List<String> ids;
    private final Map<String, DistancesRequestResult> map;

    TestRequestBuilder(String map_task_id_results) throws IOException {
      File res = new File(getClass().getResource("/" + map_task_id_results).getFile());
      map = (Map<String, DistancesRequestResult>) JsonGenericHandler.instance().getObjectFromJsonFile(JsonContainer.class, res);
      ids = new ArrayList<>(map.keySet());
    }

    @Override
    public Requests.Distance buildInitDistanceComputationRequest(ExecutorContext context) {
      String taskId = ids.iterator().next();
      ids.remove(taskId);
      return (seg) -> new RemoteTask(seg.element.ids.get(0));
    }

    @Override
    public Requests.DistanceResult buildGetDistanceComputationResultRequest(String remoteServiceUrl) {
      return (taskId) -> {
        ObjectMapper mapper = new ObjectMapper();
        String body = mapper.writer().writeValueAsString(map.get(taskId));
        return Response.builder()
          .body(body, Charset.defaultCharset())
          .status(200)
          .headers(new HashMap<>())
          .build();
      };
    }
  }
}
