package com.orange.documentare.app.ncdremote.request;

import com.orange.documentare.app.ncdremote.ExecutorContext;

public interface RequestBuilder {
  Requests.Distance buildInitDistanceComputationRequest(ExecutorContext context);
  Requests.DistanceResult buildGetDistanceComputationResultRequest(String remoteServiceUrl);
}
