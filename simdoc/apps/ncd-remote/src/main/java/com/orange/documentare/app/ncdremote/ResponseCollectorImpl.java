package com.orange.documentare.app.ncdremote;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.google.common.collect.ImmutableList;
import com.orange.documentare.app.ncdremote.MatrixDistancesSegments.MatrixDistancesSegment;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class ResponseCollectorImpl implements ResponseCollector<MatrixDistancesSegment> {

  private final List<MatrixDistancesSegment> segments = new ArrayList<>();
  private final int expectedCount;

  @Override
  public synchronized void add(MatrixDistancesSegment matrixDistancesSegment) {
    segments.add(matrixDistancesSegment);
  }

  @Override
  public synchronized List<MatrixDistancesSegment> responses() {
    return ImmutableList.copyOf(segments);
  }

  @Override
  public boolean allResponsesCollected() {
    return segments.size() == expectedCount;
  }
}
