package com.orange.documentare.app.ncdremote;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import feign.*;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;

@Slf4j
class KillAllTasks {
  interface IKillAllTasks {
    @RequestLine("POST /kill-all-tasks")
    void killAllTasks();
  }

  static void serialKiller(File rHosts) throws IOException {
    LocalAvailableRemoteServices services = new LocalAvailableRemoteServices(rHosts);
    services.services().forEach(service -> kill(service.url));
    try {
      Thread.sleep(2000);
    } catch (InterruptedException e) {
      // catch silently
    }
  }

  private static void kill(String url) {
    try {
      log.info("Kill all tasks @ " + url);
      buildFeignRequest(url)
              .killAllTasks();
    } catch (FeignException f) {
      log.warn("Request kill-all-tasks on Service {} failed with status {}", url, f.status());
    }
  }

  private static IKillAllTasks buildFeignRequest(String url) {
    return Feign.builder()
            .retryer(Retryer.NEVER_RETRY)
            .options(new Request.Options(5000, 5000))
            .target(KillAllTasks.IKillAllTasks.class, url);
  }
}
