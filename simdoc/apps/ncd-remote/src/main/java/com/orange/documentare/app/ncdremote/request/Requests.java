package com.orange.documentare.app.ncdremote.request;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.orange.documentare.app.ncdremote.ExecutorContext;
import com.orange.documentare.app.ncdremote.MatrixDistancesSegments;
import com.orange.documentare.app.ncdremote.RemoteTask;
import feign.*;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;

public class Requests implements RequestBuilder {

  public interface Distance {
    @RequestLine("POST /distances")
    @Headers("Content-Type: application/json")
    RemoteTask distance(MatrixDistancesSegments.MatrixDistancesSegment segment);
  }

  public interface DistanceResult {
    @RequestLine("GET /task/{taskId}")
    Response distanceResult(@Param("taskId") String taskId) throws JsonProcessingException;
  }

  @Override
  public Requests.Distance buildInitDistanceComputationRequest(ExecutorContext context) {
    return Feign.builder()
            .options(new Request.Options(5000, 5000))
            .retryer(Retryer.NEVER_RETRY)
            .encoder(new JacksonEncoder())
            .decoder(new JacksonDecoder())
            .target(Requests.Distance.class, context.remoteService.url);
  }

  @Override
  public Requests.DistanceResult buildGetDistanceComputationResultRequest(String remoteServiceUrl) {
    return Feign.builder()
            .encoder(new JacksonEncoder())
            .decoder(new JacksonDecoder())
            .target(Requests.DistanceResult.class, remoteServiceUrl);
  }
}
