package com.orange.documentare.app.ncdremote;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.google.common.collect.ImmutableList;
import com.orange.documentare.core.model.json.JsonGenericHandler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LocalAvailableRemoteServices implements AvailableRemoteServices {
  private List<RemoteService> availableServices = new ArrayList<>();
  private static JsonGenericHandler jsonGenericHandler = new JsonGenericHandler(true);

  LocalAvailableRemoteServices(File rHosts) throws IOException {
    availableServices = Arrays.asList(loadRemoteServices(rHosts));
  }

  @Override
  public void update() {
    // FIXME, in next version:
    // - here we should clear the available services
    // - and update it through a request the service discovery server
  }

  @Override
  public List<RemoteService> services() {
    return ImmutableList.copyOf(availableServices);
  }

  private static RemoteService[] loadRemoteServices(File rHosts) throws IOException {
    RemoteService[] remoteServices = (RemoteService[]) jsonGenericHandler.getObjectFromJsonFile(RemoteService[].class, rHosts);
    return remoteServices;
  }

}
