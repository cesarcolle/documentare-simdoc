package com.orange.documentare.simdoc.server.biz.clustering;

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.simdoc.server.biz.RemoteTask;
import com.orange.documentare.simdoc.server.biz.SharedDirectory;
import com.orange.documentare.simdoc.server.biz.task.TaskController;
import com.orange.documentare.simdoc.server.biz.task.Tasks;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class ClusteringFewItemsTest {
  private static final String OUTPUT_DIRECTORY = "out";

  @Before
  public void setup() {
    cleanup();
    new File(OUTPUT_DIRECTORY).mkdir();
  }

  @After
  public void cleanup() {
    FileUtils.deleteQuietly(new File(OUTPUT_DIRECTORY));
  }


  @Test
  public void build_clustering_with_no_items() throws Exception {
    // given
    BytesDataArray bytesDataArray = new BytesDataArray(new BytesData[0]);

    ClusteringRequest req = ClusteringRequest.builder()
      .bytesData(bytesDataArray.bytesData)
      .outputDirectory(OUTPUT_DIRECTORY)
      .debug()
      .build();

    // when
    Clustering cl = test(req);

    // then
    assertThat(cl.elements).hasSize(0);
  }

  @Test
  public void build_clustering_with_one_item() throws Exception {
    // Given
    BytesDataArray bytesDataArray = new BytesDataArray(new BytesData[]{
      new BytesData(new ArrayList<>(Arrays.asList("0")), new byte[]{0})
    });

    ClusteringRequest req = ClusteringRequest.builder()
      .bytesData(bytesDataArray.bytesData)
      .outputDirectory(OUTPUT_DIRECTORY)
      .debug()
      .build();

    Clustering cl = test(req);

    assertThat(cl.elements).hasSize(1);
    assertThat(cl.elements.get(0).clusterCenter()).isTrue();
    assertThat(cl.elements.get(0).getClusterId()).isEqualTo(0);
  }

  @Test
  public void build_clustering_with_two_items() throws Exception {
    // Given
    BytesDataArray bytesDataArray = new BytesDataArray(new BytesData[]{
      new BytesData(new ArrayList<>(Arrays.asList("0")), new byte[]{0}),
      new BytesData(new ArrayList<>(Arrays.asList("1")), new byte[]{1})
    });

    ClusteringRequest req = ClusteringRequest.builder()
      .bytesData(bytesDataArray.bytesData)
      .outputDirectory(OUTPUT_DIRECTORY)
      .debug()
      .build();

    Clustering cl = test(req);

    assertThat(cl.elements).hasSize(2);
    assertThat(cl.elements.get(0).clusterCenter()).isTrue();
    assertThat(cl.elements.get(1).clusterCenter()).isFalse();
    assertThat(cl.elements.get(0).getClusterId()).isEqualTo(0);
    assertThat(cl.elements.get(1).getClusterId()).isEqualTo(0);
    assertThat(cl.elements.get(1).distanceToCenter).isEqualTo(125000);
  }

  private Clustering test(ClusteringRequest req) throws IOException, InterruptedException {
    Tasks tasks = new Tasks();
    TaskController taskController = new TaskController();
    taskController.setTasks(tasks);

    ClusteringController clusteringController = new ClusteringController();
    clusteringController.tasks = tasks;
    clusteringController.clusteringService = new ClusteringServiceImpl();
    clusteringController.sharedDirectory = new SharedDirectory();

    RemoteTask remoteTask = clusteringController.clustering(req, new MockHttpServletResponse());

    // when
    Clustering clustering;
    do {
      Thread.sleep(100);
      clustering = (Clustering) taskController.task(remoteTask.id, new MockHttpServletResponse());
    } while (clustering == null);

    return clustering;
  }
}
