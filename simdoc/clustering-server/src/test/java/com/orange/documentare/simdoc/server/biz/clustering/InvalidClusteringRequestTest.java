package com.orange.documentare.simdoc.server.biz.clustering;

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.simdoc.server.biz.RemoteTask;
import com.orange.documentare.simdoc.server.biz.SharedDirectory;
import com.orange.documentare.simdoc.server.biz.task.TaskController;
import com.orange.documentare.simdoc.server.biz.task.Tasks;
import org.apache.commons.io.FileUtils;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.File;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class InvalidClusteringRequestTest {
  private static final String OUTPUT_DIRECTORY = "out";

  @Before
  public void setup() {
    cleanup();
    new File(OUTPUT_DIRECTORY).mkdir();
  }

  @After
  public void cleanup() {
    FileUtils.deleteQuietly(new File(OUTPUT_DIRECTORY));
  }


  @Test
  public void clustering_api_return_bad_request_if_bytes_data_is_missing() throws Exception {
    // Given
    String expectedMessage = "bytesData is missing";
    ClusteringRequest req = ClusteringRequest.builder()
      .outputDirectory(OUTPUT_DIRECTORY)
      .build();

    test(req, expectedMessage);
  }


  @Test
  public void clustering_api_return_bad_request_if_output_directory_is_missing() throws Exception {
    // Given
    String expectedMessage = "outputDirectory is missing";
    BytesData[] bytesData = BytesData.loadFromDirectory(new File(inputDirectory()), File::getName);
    ClusteringRequest req = ClusteringRequest.builder()
      .bytesData(bytesData)
      .build();

    test(req, expectedMessage);
  }

  @Test
  public void clustering_api_return_bad_request_if_output_directory_is_not_a_directory() throws Exception {
    // Given
    String expectedMessage = "outputDirectory is not a directory: ";
    File outputDirectory = new File(OUTPUT_DIRECTORY);
    FileUtils.deleteQuietly(outputDirectory);
    FileUtils.writeStringToFile(outputDirectory, "hi");
    BytesData[] bytesData = BytesData.loadFromDirectory(new File(inputDirectory()), File::getName);
    ClusteringRequest req = ClusteringRequest.builder()
      .bytesData(bytesData)
      .outputDirectory(OUTPUT_DIRECTORY)
      .build();

    test(req, expectedMessage);
  }

  @Test
  public void clustering_api_return_bad_request_if_output_directory_is_not_writable() throws Exception {
    // Given
    BytesData[] bytesData = BytesData.loadFromDirectory(new File(inputDirectory()), File::getName);
    String expectedMessage = "outputDirectory is not writable: /";
    ClusteringRequest req = ClusteringRequest.builder()
      .bytesData(bytesData)
      .outputDirectory("/")
      .build();

    test(req, expectedMessage);
  }


  private String inputDirectory() {
    return new File(getClass().getResource("/glyphs").getFile()).getAbsolutePath();
  }

  private void test(ClusteringRequest req, String expectedMessage) throws IOException {
    Tasks tasks = new Tasks();
    TaskController taskController = new TaskController();
    taskController.setTasks(tasks);

    ClusteringController clusteringController = new ClusteringController();
    clusteringController.tasks = tasks;
    clusteringController.clusteringService = new ClusteringServiceImpl();
    clusteringController.sharedDirectory = new SharedDirectory();

    MockHttpServletResponse res = new MockHttpServletResponse();
    clusteringController.clustering(req, res);

    assertThat(res.getStatus()).isEqualTo(400);
    assertThat(res.getErrorMessage()).contains(expectedMessage);
  }
}
