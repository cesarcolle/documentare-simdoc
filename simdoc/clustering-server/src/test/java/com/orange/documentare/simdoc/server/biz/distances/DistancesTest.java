package com.orange.documentare.simdoc.server.biz.distances;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.simdoc.server.biz.RemoteTask;
import com.orange.documentare.simdoc.server.biz.task.TaskController;
import com.orange.documentare.simdoc.server.biz.task.Tasks;
import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DistancesTest {

  private static final String[] ANIMALS = {
    "pigmyChimpanzee", "chimpanzee", "human"
  };
  private static final int[] EXPECTED_DISTANCES = {
    547959, 552673, 0
  };

  @Test
  public void compute_human_to_animals_distances() throws Exception {
    // Given
    BytesData element = load("human");
    BytesData[] elements = loadAnimals();

    DistancesRequest req = DistancesRequest.builder()
      .element(element)
      .compareTo(elements)
      .build();

    // when
    DistancesRequestResult result = test(req);

    // then
    IntStream.range(0, ANIMALS.length).forEach(i ->
      Assertions.assertThat(result.distances[i]).isEqualTo(EXPECTED_DISTANCES[i])
    );
  }

  private BytesData load(String id) {
    File file = new File(getClass().getResource("/animals-dna/" + id).getFile());
    return new BytesData(List.of(id).toJavaList(), List.of(file.getAbsolutePath()).toJavaList());
  }

  private BytesData[] loadAnimals() {
    return Arrays.stream(ANIMALS)
      .map(this::load)
      .collect(Collectors.toList())
      .toArray(new BytesData[ANIMALS.length]);
  }

  private DistancesRequestResult test(DistancesRequest req) throws IOException, InterruptedException {
    Tasks tasks = new Tasks();
    TaskController taskController = new TaskController();
    taskController.setTasks(tasks);

    DistancesController distancesController = new DistancesController();
    distancesController.tasks = tasks;

    RemoteTask remoteTask = distancesController.distances(req, new MockHttpServletResponse());

    // when
    DistancesRequestResult distancesRequestResult;
    do {
      Thread.sleep(100);
      distancesRequestResult = (DistancesRequestResult) taskController.task(remoteTask.id, new MockHttpServletResponse());
    } while (distancesRequestResult == null);

    return distancesRequestResult;
  }
}
