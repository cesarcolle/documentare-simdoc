package com.orange.documentare.simdoc.server.biz.clustering;

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.simdoc.server.biz.FileIO;
import com.orange.documentare.simdoc.server.biz.RemoteTask;
import com.orange.documentare.simdoc.server.biz.SharedDirectory;
import com.orange.documentare.simdoc.server.biz.task.TaskController;
import com.orange.documentare.simdoc.server.biz.task.Tasks;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.File;
import java.io.IOException;

public class ClusteringParametersTest {
  private static final String OUTPUT_DIRECTORY = "out";

  @Before
  public void setup() {
    cleanup();
    new File(OUTPUT_DIRECTORY).mkdir();
  }

  @After
  public void cleanup() {
    FileUtils.deleteQuietly(new File(OUTPUT_DIRECTORY));
  }


  @Test
  public void call_service_with_default_parameters() throws Exception {
    // Given
    BytesData[] bytesData = BytesData.loadFromDirectory(new File(inputDirectory()), File::getName);
    ClusteringRequest req = ClusteringRequest.builder()
      .bytesData(bytesData)
      .outputDirectory(OUTPUT_DIRECTORY)
      .build();

    // When/Then
    test(req);
  }

  @Test
  public void call_service_with_debug() throws Exception {
    // Given
    BytesData[] bytesData = BytesData.loadFromDirectory(new File(inputDirectory()), File::getName);
    ClusteringRequest req = ClusteringRequest.builder()
      .bytesData(bytesData)
      .outputDirectory(OUTPUT_DIRECTORY)
      .debug()
      .build();

    // When/Then
    test(req);
  }

  @Test
  public void call_service_with_parameters() throws Exception {
    // Given
    float acut = 1.1f;
    float qcut = 2.1f;
    float scut = 3.1f;
    int ccut = 4;
    int k = 6;
    BytesData[] bytesData = BytesData.loadFromDirectory(new File(inputDirectory()), File::getName);
    ClusteringRequest req = ClusteringRequest.builder()
      .bytesData(bytesData)
      .outputDirectory(OUTPUT_DIRECTORY)
      .acut(acut)
      .qcut(qcut)
      .scut(scut)
      .ccut(ccut)
      .wcut()
      .k(k)
      .sloop()
      .allInSameCluster()
      .build();

    // When/Then
    test(req);
  }

  private String inputDirectory() {
    return new File(getClass().getResource("/glyphs").getFile()).getAbsolutePath();
  }

  private void test(ClusteringRequest req) throws IOException, InterruptedException {
    Tasks tasks = new Tasks();
    TaskController taskController = new TaskController();
    taskController.setTasks(tasks);

    ClusteringService clusteringService = Mockito.mock(ClusteringService.class);
    Mockito.when(clusteringService.build(Mockito.any(), Mockito.any())).thenReturn(Clustering.error(null, null));

    ClusteringController clusteringController = new ClusteringController();
    clusteringController.tasks = tasks;
    clusteringController.clusteringService = clusteringService;
    clusteringController.sharedDirectory = new SharedDirectory();

    RemoteTask remoteTask = clusteringController.clustering(req, new MockHttpServletResponse());

    // when
    Clustering clustering;
    do {
      Thread.sleep(100);
      clustering = (Clustering) taskController.task(remoteTask.id, new MockHttpServletResponse());
    } while (clustering == null);

    Mockito.verify(clusteringService).build(
      new FileIO(clusteringController.sharedDirectory, req),
      req);
  }
}
