package com.orange.documentare.simdoc.server.biz.clustering;

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.model.json.JsonGenericHandler;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.simdoc.server.biz.RemoteTask;
import com.orange.documentare.simdoc.server.biz.SharedDirectory;
import com.orange.documentare.simdoc.server.biz.task.TaskController;
import com.orange.documentare.simdoc.server.biz.task.Tasks;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ClusteringGlyphsTest {
  private static final String OUTPUT_DIRECTORY = "out";

  @Before
  public void setup() {
    cleanup();
    new File(OUTPUT_DIRECTORY).mkdir();
  }

  @After
  public void cleanup() {
    FileUtils.deleteQuietly(new File(OUTPUT_DIRECTORY));
  }

  @Test
  public void build_glyphs_clustering_with_bytes_data() throws Exception {
    // Given
    File inputDirectory = new File(inputDirectory());
    BytesData[] bytesData = BytesData.loadFromDirectory(inputDirectory, BytesData.relativePathIdProvider(inputDirectory));
    ClusteringRequest req = ClusteringRequest.builder()
      .bytesData(bytesData)
      .outputDirectory(OUTPUT_DIRECTORY)
      .debug()
      .build();

    test(req, "/expected-clustering-result-glyphs.json");
  }

  private String inputDirectory() {
    return new File(getClass().getResource("/glyphs").getFile()).getAbsolutePath();
  }

  private Clustering expectedClusteringResult(String expectedJson) throws IOException {
    String expectedFile = getClass().getResource(expectedJson).getFile();
    return (Clustering) JsonGenericHandler.instance().getObjectFromJsonFile(Clustering.class, new File(expectedFile));
  }

  private void test(ClusteringRequest req, String expectedJson) throws IOException, InterruptedException {
    Tasks tasks = new Tasks();
    TaskController taskController = new TaskController();
    taskController.setTasks(tasks);

    ClusteringController clusteringController = new ClusteringController();
    clusteringController.tasks = tasks;
    clusteringController.clusteringService = new ClusteringServiceImpl();
    clusteringController.sharedDirectory = new SharedDirectory();

    RemoteTask remoteTask = clusteringController.clustering(req, new MockHttpServletResponse());

    // when
    Clustering clustering;
    do {
      Thread.sleep(100);
      clustering = (Clustering) taskController.task(remoteTask.id, new MockHttpServletResponse());
    } while(clustering == null);

    // then

    // these fields are not serialized
    clustering.graph.getItems().forEach(graphItem -> {
      graphItem.setVertex1(null);
      graphItem.setVertex2(null);
      graphItem.setVertex3(null);
    });

    List<String> outputDirectoryList = Arrays.asList(new File(OUTPUT_DIRECTORY).list());
    assertThat(outputDirectoryList).contains("clustering-request.json.gz");
    assertThat(outputDirectoryList).contains("clustering-graph.json.gz");
    assertThat(outputDirectoryList).contains("clustering-result.json.gz");

    Clustering expected = expectedClusteringResult(expectedJson);
    assertThat(clustering).isEqualTo(expected);
  }
}
