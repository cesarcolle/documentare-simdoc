package com.orange.documentare.simdoc.server.biz.task;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.simdoc.server.biz.distances.DistancesRequestResult;
import io.vavr.control.Try;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.stream.IntStream;

import static java.lang.System.out;
import static org.assertj.core.api.Assertions.assertThat;

public class TasksTest {

  @Test
  public void task_id_is_unique() {
    // Given
    Tasks tasks = new Tasks();

    // When
    String id1 = tasks.newTask();
    String id2 = tasks.newTask();

    // Then
    assertThat(id1).isNotEqualTo(id2);
  }

  @Test
  public void invalid_task_id() {
    // Given
    Tasks tasks = new Tasks();

    // When
    tasks.isDone("invalid");

    // Then
    assertThat(tasks.exists("invalid")).isFalse();
  }

  @Test
  public void add_task_then_flag_it_as_finished_then_remove_it_when_retrieved() {
    // Given
    Tasks tasks = new Tasks();
    Object result = new Object();

    // When / Then
    String id = tasks.newTask();
    out.println("task ids = " + id);

    assertThat(tasks.isDone(id)).isFalse();
    tasks.addResult(id, result);
    assertThat(tasks.isDone(id)).isTrue();

    Task task = tasks.pop(id);
    assertThat(tasks.present(id)).isFalse();
    assertThat(task.result.get()).isEqualTo(result);
  }

  @Test
  public void add_error_result() {
    // Given
    Tasks tasks = new Tasks();
    DistancesRequestResult result = DistancesRequestResult.error("err");
    String id = tasks.newTask();

    // When
    tasks.addErrorResult(id, result);
    Task task = tasks.pop(id);

    // Then
    assertThat(task.error).isTrue();
  }

  @Test
  public void reach_max_number_of_tasks() {
    // Given
    Tasks tasks = new Tasks();

    // When / Then
    IntStream.range(0, 2).forEach(j -> {

      IntStream.range(0, Runtime.getRuntime().availableProcessors()).forEach(i -> {
          assertThat(tasks.canAcceptNewTask()).isTrue();
          tasks.run(() -> {
            do { out.print("*");} while (true);
          }, "");
        }
      );

      assertThat(tasks.canAcceptNewTask()).isFalse();
      Try.run(() -> Thread.sleep(10));
      tasks.killAll();
    });
  }
}
