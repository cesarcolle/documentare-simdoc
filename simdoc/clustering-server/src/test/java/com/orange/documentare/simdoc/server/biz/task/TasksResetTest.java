package com.orange.documentare.simdoc.server.biz.task;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.simdoc.server.biz.RemoteTask;
import com.orange.documentare.simdoc.server.biz.distances.DistancesController;
import com.orange.documentare.simdoc.server.biz.distances.DistancesRequest;
import com.orange.documentare.simdoc.server.biz.distances.DistancesRequestResult;
import io.vavr.collection.List;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TasksResetTest {

  private static final String[] ANIMALS = {
    "pigmyChimpanzee", "chimpanzee", "human"
  };

  @Test
  public void kill_all_tasks() throws Exception {
    // Given
    BytesData element = load("human");
    BytesData[] elements = loadAnimals();

    DistancesRequest req = DistancesRequest.builder()
      .element(element)
      .compareTo(elements)
      .build();

    Tasks tasks = new Tasks();
    TaskController taskController = new TaskController();
    taskController.setTasks(tasks);

    DistancesController distancesController = new DistancesController();
    distancesController.setTasks(tasks);

    RemoteTask remoteTask = distancesController.distances(req, new MockHttpServletResponse());

    // when
    taskController.killAll();

    // then
    assertThat(tasks.exists(remoteTask.id)).isFalse();
  }

  private BytesData load(String id) {
    File file = new File(getClass().getResource("/animals-dna/" + id).getFile());
    return new BytesData(List.of(id).toJavaList(), List.of(file.getAbsolutePath()).toJavaList());
  }

  private BytesData[] loadAnimals() {
    return Arrays.stream(ANIMALS)
      .map(this::load)
      .collect(Collectors.toList())
    .toArray(new BytesData[ANIMALS.length]);
  }
}
