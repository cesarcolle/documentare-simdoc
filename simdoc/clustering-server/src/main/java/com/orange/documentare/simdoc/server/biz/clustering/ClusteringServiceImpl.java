package com.orange.documentare.simdoc.server.biz.clustering;

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.clustering.graph.graphbuilder.ClusteringGraphBuilder;
import com.orange.documentare.core.comp.distance.DistancesArray;
import com.orange.documentare.core.comp.distance.Duplicates;
import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.comp.distance.bytesdistances.BytesDistances;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.core.model.ref.clustering.ClusteringElement;
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters;
import com.orange.documentare.core.model.ref.comp.NearestItem;
import com.orange.documentare.core.model.ref.comp.TriangleVertices;
import com.orange.documentare.simdoc.server.biz.FileIO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ClusteringServiceImpl implements ClusteringService {

  @RequiredArgsConstructor
  private class DistancesComputationResult {
    public final String[] ids;
    public final DistancesArray distancesArray;
  }

  @Override
  public Clustering build(FileIO fileIO, ClusteringRequest clusteringRequest) throws IOException {
    fileIO.deleteAllClusteringFiles();
    fileIO.writeRequest(clusteringRequest);

    Clustering clustering = buildClustering(clusteringRequest);

    if (clusteringRequest.debug()) {
      fileIO.writeClusteringResult(clustering);
    }
    return clustering;
  }

  private Clustering buildClustering(ClusteringRequest clusteringRequest) {
    DistancesComputationResult distancesComputationResult = computeDistances(clusteringRequest.bytesData);
    Duplicates duplicates = distancesComputationResult.distancesArray.duplicates();
    DistancesComputationResult distancesWithoutDuplicates = distancesWithoutDuplicates(duplicates, distancesComputationResult);

    SimClusteringItem[] simClusteringItems = initClusteringItems(distancesWithoutDuplicates, clusteringRequest.clusteringParameters());

    ClusteringGraphBuilder clusteringGraphBuilder = new ClusteringGraphBuilder();
    Clustering cl = clusteringGraphBuilder.doClustering(simClusteringItems, clusteringRequest.clusteringParameters());

    List<ClusteringElement> clusterCenters = cl.elements
      .stream()
      .filter(el -> el.clusterCenter())
      .collect(Collectors.toList());

    Map<Integer, List<ClusteringElement>> allFilesInClusterByClusterId = cl.elements.stream()
      .collect(Collectors.groupingBy(ClusteringElement::getClusterId));

    List<ClusteringElement> finalElements = new ArrayList<>();
    for(ClusteringElement clusterCenter:clusterCenters) {
      List<ClusteringElement> allFilesInCluster = allFilesInClusterByClusterId.get(clusterCenter.clusterId);
      for(ClusteringElement fileInCluster:allFilesInCluster) {
        String fileId = fileInCluster.id;
        String centerId = clusterCenter.id;
        int fileIndice = 0;
        int centerIndice = 0;
        int i = 0;
        for(String id : distancesWithoutDuplicates.ids) {
          if (id == fileInCluster.id) {
            fileIndice = i;
          }
          if (id == centerId) {
            centerIndice = i;
          }
          i++;
        }
        if (fileIndice != centerIndice) {
          int distanceToCenter = distancesWithoutDuplicates.distancesArray.get(fileIndice, centerIndice);
          log.info("fileId = {}, fileIndice = {}, centerId = {}, clusterIndice = {}, distanceToCenter = {}", fileId, fileIndice, centerId, centerIndice, distanceToCenter);
          ClusteringElement fileWithDistanceToCenter = new ClusteringElement(fileInCluster.id, fileInCluster.clusterId, fileInCluster.clusterCenter, fileInCluster.enrolled, fileInCluster.duplicateOf, distanceToCenter);
          finalElements.add(fileWithDistanceToCenter);
        } else {
          finalElements.add(fileInCluster);
        }
      }
    }

    cl = new Clustering(finalElements, cl.subgraphs, cl.clusters, cl.parameters, cl.graph, cl.error);

    return duplicates.count() == 0 ?
      cl : duplicates.addDuplicatesToClustering(cl, distancesComputationResult.ids);
  }

  private DistancesComputationResult computeDistances(BytesData[] bytesDataArray) {
    BytesDistances bytesDistances = new BytesDistances();
    DistancesArray distanceArray = bytesDistances.computeDistancesInCollection(bytesDataArray);
    String[] ids = Arrays.stream(bytesDataArray)
      .map(bytesData -> bytesData.ids.get(0))
      .toArray(String[]::new);
    return new DistancesComputationResult(ids, distanceArray);
  }

  private DistancesComputationResult distancesWithoutDuplicates(Duplicates duplicates, DistancesComputationResult distances) {
    if (duplicates.count() == 0) {
      return distances;
    }

    DistancesArray distancesArrayWithoutDuplicates = distances.distancesArray.removeDuplicates();
    String[] ids = distances.ids;

    String[] idsWithoutDuplicates = duplicates.indicesWithoutDuplicates(ids.length).stream()
      .map(index -> ids[index])
      .toArray(String[]::new);

    return new DistancesComputationResult(idsWithoutDuplicates, distancesArrayWithoutDuplicates);
  }

  private SimClusteringItem[] initClusteringItems(
    DistancesComputationResult distancesComputationResult, ClusteringParameters parameters) {
    String[] ids = distancesComputationResult.ids;
    int nbItems = ids.length;
    int k = parameters.knn() ? parameters.kNearestNeighboursThreshold : nbItems;
    SimClusteringItem[] simClusteringItems = new SimClusteringItem[nbItems];
    for(int i = 0; i < nbItems; i++) {
      simClusteringItems[i] = new SimClusteringItem(ids[i]);
    }
    buildTriangulationVertices(simClusteringItems, distancesComputationResult.distancesArray, k);
    return simClusteringItems;
  }

  /** Memory in place creation, it is optimal since we do not allocate nearest arrays */
  private void buildTriangulationVertices(SimClusteringItem[] simClusteringItems, DistancesArray distancesArray, int k) {
    if (simClusteringItems.length < 3) {
      return;
    }

    List<SimClusteringItem> itemsList = Arrays.asList(simClusteringItems);
    for (int i = 0; i < simClusteringItems.length; i++) {
      NearestItem vertex2 = distancesArray.nearestItemOf(i);
      NearestItem vertex3 = distancesArray.nearestItemOfBut(vertex2.getIndex(), i);
      NearestItem[] vertex1Nearest = distancesArray.nearestItemsFor(itemsList, i);

      SimClusteringItem item = simClusteringItems[i];

      // FIXME we keep nearest items for singletons experimentation on Jo's side
      item.setNearestItems(vertex1Nearest);
      item.setTriangleVertices(new TriangleVertices(vertex1Nearest, vertex3, k));
    }
  }
}
