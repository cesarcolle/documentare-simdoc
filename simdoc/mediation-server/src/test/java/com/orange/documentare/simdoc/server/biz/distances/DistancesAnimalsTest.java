package com.orange.documentare.simdoc.server.biz.distances;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.image.opencv.OpencvLoader;
import com.orange.documentare.core.model.json.JsonGenericHandler;
import com.orange.documentare.simdoc.server.biz.RemoteTask;
import com.orange.documentare.simdoc.server.biz.SharedDirectory;
import com.orange.documentare.simdoc.server.biz.distances.remoteclient.RemoteDistance;
import com.orange.documentare.simdoc.server.biz.prepdata.AppPrepData;
import com.orange.documentare.simdoc.server.biz.task.TaskController;
import com.orange.documentare.simdoc.server.biz.task.Tasks;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.apache.commons.io.FileUtils;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.zip.CRC32;

import static org.assertj.core.api.Assertions.assertThat;

public class DistancesAnimalsTest {

  private static final String[] ANIMALS = {
    "pigmyChimpanzee", "chimpanzee", "platypus", "human"
  };
  private static final String ANIMALS_CLUSTERING_SERVER_RESPONSE = "{\n" +
    " \"distances\" : [ 547959, 552673, 950527, 0 ],\n" +
    " \"error\" : false\n" +
    "}";
  private static final int[] ANIMALS_EXPECTED_DISTANCES = {
    547959, 552673, 950527, 0
  };

  @Rule
  public MockWebServer server = new MockWebServer();


  @Test
  public void compute_human_to_animals_distances() throws Exception {
    // given
    BytesData[] element = {loadAnimal("human")};
    BytesData[] elements = loadAnimals();

    DistancesRequest req = DistancesRequest.builder()
      .element(element)
      .compareTo(elements)
      .build();

    server.enqueue(new MockResponse().setBody("{\"id\": \"1234\"}"));
    server.enqueue(new MockResponse().setBody(ANIMALS_CLUSTERING_SERVER_RESPONSE));

    // when
    Tuple2<String, DistancesRequestResult> result = test(req);

    // then
    IntStream.range(0, ANIMALS.length).forEach(i ->
      assertThat(result._2.distances[i]).isEqualTo(ANIMALS_EXPECTED_DISTANCES[i])
    );
  }

  private Tuple2<String, DistancesRequestResult> test(DistancesRequest req) throws IOException, InterruptedException {
    return distanceTest(req, server.url("/"));
  }

  static Tuple2<String, DistancesRequestResult> distanceTest(DistancesRequest req, HttpUrl url) throws IOException, InterruptedException {
    Tasks tasks = new Tasks();
    TaskController taskController = new TaskController();
    taskController.setTasks(tasks);

    DistancesController distancesController = new DistancesController();
    distancesController.setTasks(tasks);
    AppPrepData appPrepData = new AppPrepData();
    appPrepData.setSharedDirectory(new SharedDirectory());
    distancesController.appPrepData = appPrepData;
    RemoteDistance remoteDistance = new RemoteDistance();
    remoteDistance.setClusteringServerUrl(url.toString());
    distancesController.remoteDistance = remoteDistance;

    RemoteTask remoteTask = distancesController.distances(req, new MockHttpServletResponse());

    DistancesRequestResult result;
    do {
      Thread.sleep(200);
      result = (DistancesRequestResult) taskController.task(remoteTask.id, new MockHttpServletResponse());
    } while (result == null);
    return Tuple.of(remoteTask.id, result);
  }

  private BytesData loadAnimal(String id) {
    return load(id, "/animals-dna/");
  }

  private BytesData load(String id, String path) {
    File file = new File(getClass().getResource(path + id).getFile());
    return new BytesData(List.of(id).toJavaList(), List.of(file.getAbsolutePath()).toJavaList());
  }

  private BytesData[] loadAnimals() {
    return Arrays.stream(ANIMALS)
      .map(this::loadAnimal)
      .collect(Collectors.toList())
      .toArray(new BytesData[ANIMALS.length]);
  }
}
