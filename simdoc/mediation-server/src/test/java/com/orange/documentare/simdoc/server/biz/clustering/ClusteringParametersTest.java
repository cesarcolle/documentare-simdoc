package com.orange.documentare.simdoc.server.biz.clustering;

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.simdoc.server.biz.FileIO;
import com.orange.documentare.simdoc.server.biz.RemoteTask;
import com.orange.documentare.simdoc.server.biz.task.TaskController;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.File;
import java.io.IOException;

import static com.orange.documentare.simdoc.server.biz.clustering.ClusteringAnimalsTest.setupClusteringController;
import static com.orange.documentare.simdoc.server.biz.clustering.ClusteringAnimalsTest.setupTaskController;

public class ClusteringParametersTest {
  private static final String INPUT_DIRECTORY = "in";
  private static final String OUTPUT_DIRECTORY = "out";

  @Before
  public void setup() {
    cleanup();
  }

  @After
  public void cleanup() {
    FileUtils.deleteQuietly(new File(INPUT_DIRECTORY));
    FileUtils.deleteQuietly(new File(OUTPUT_DIRECTORY));
  }

  @Test
  public void call_service_with_default_parameters() throws Exception {
    // Given
    ClusteringRequest req = ClusteringRequest.builder()
      .inputDirectory(INPUT_DIRECTORY)
      .outputDirectory(OUTPUT_DIRECTORY)
      .build();

    // When/Then
    test(req);
  }

  @Test
  public void call_service_with_debug() throws Exception {
    // Given
    ClusteringRequest req = ClusteringRequest.builder()
      .inputDirectory(INPUT_DIRECTORY)
      .outputDirectory(OUTPUT_DIRECTORY)
      .debug(true)
      .build();

    // When/Then
    test(req);
  }

  @Test
  public void call_service_with_parameters() throws Exception {
    // Given
    float acut = 1.1f;
    float qcut = 2.1f;
    float scut = 3.1f;
    int ccut = 4;
    int k = 6;
    ClusteringRequest req = ClusteringRequest.builder()
      .inputDirectory(INPUT_DIRECTORY)
      .outputDirectory(OUTPUT_DIRECTORY)
      .acutSdFactor(acut)
      .qcutSdFactor(qcut)
      .scutSdFactor(scut)
      .ccutPercentile(ccut)
      .wcut(true)
      .kNearestNeighboursThreshold(k)
      .sloop(true)
      .build();

    // When/Then
    test(req);
  }

  private void test(ClusteringRequest req) throws InterruptedException, IOException {
    createInputDirectory();
    createOutputDirectory();

    TaskController taskController = setupTaskController();
    ClusteringController clusteringController = setupClusteringController("", taskController);
    clusteringController.clusteringService = Mockito.mock(ClusteringService.class);

    // when
    RemoteTask remoteTask = clusteringController.clustering(req, new MockHttpServletResponse());

    Clustering clustering;
    do {
      Thread.sleep(300);
      clustering = (Clustering) taskController.task(remoteTask.id, new MockHttpServletResponse());
    } while (clustering == null);

    Mockito.verify(clusteringController.clusteringService).build(
      remoteTask.id,
      (new FileIO(clusteringController.sharedDirectory, req)).forTaskId(remoteTask.id),
      req);
  }


  private void createInputDirectory() {
    (new File(INPUT_DIRECTORY)).mkdir();
  }

  private void createOutputDirectory() {
    (new File(OUTPUT_DIRECTORY)).mkdir();
  }
}
