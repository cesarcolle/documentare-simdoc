package com.orange.documentare.simdoc.server.biz.clustering;

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.model.json.JsonGenericHandler;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.simdoc.server.biz.RemoteTask;
import com.orange.documentare.simdoc.server.biz.SharedDirectory;
import com.orange.documentare.simdoc.server.biz.clustering.remoteclient.RemoteClustering;
import com.orange.documentare.simdoc.server.biz.prepdata.AppPrepData;
import com.orange.documentare.simdoc.server.biz.task.TaskController;
import com.orange.documentare.simdoc.server.biz.task.Tasks;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ClusteringAnimalsTest {

  /** set to true to run against a real clustering server */
  private static final boolean INTEGRATION_TEST = false;
  private static final String OUTPUT_DIRECTORY = "out";

  @Rule
  public MockWebServer server = new MockWebServer();

  @Before
  public void setup() {
    cleanup();
    new File(OUTPUT_DIRECTORY).mkdir();
  }

  @After
  public void cleanup() {
    FileUtils.deleteQuietly(new File(OUTPUT_DIRECTORY));
  }


  @Test
  public void build_animals_dna_clustering() throws Exception {
    // Given
    ClusteringRequest req = ClusteringRequest.builder()
      .inputDirectory(inputDirectory())
      .outputDirectory(outputDirAbsolutePath())
      .build();

    test(req, "clustering-server-response-animals-dna.json", "expected-clustering-result-animals-dna.json", false);
  }

  @Test
  public void build_animals_dna_clustering_in_debug_mode() throws Exception {
    // Given
    ClusteringRequest req = ClusteringRequest.builder()
      .inputDirectory(inputDirectory())
      .outputDirectory(outputDirAbsolutePath())
      .debug(true)
      .build();

    test(req, "clustering-server-response-animals-dna.json", "expected-clustering-result-animals-dna.json", true);
  }

  @Test
  public void build_animals_dna_clustering_with_bytes_data_bytes_in_debug_mode() throws Exception {
    // Given
    JsonGenericHandler jsonGenericHandler = new JsonGenericHandler();
    File bytesDataJson = new File(getClass().getResource("/bytes-data-bytes-animals-dna.json").getFile());
    BytesDataArray bytesDataArray = (BytesDataArray) jsonGenericHandler.getObjectFromJsonFile(BytesDataArray.class, bytesDataJson);

    ClusteringRequest req = ClusteringRequest.builder()
      .bytesData(bytesDataArray.bytesData)
      .outputDirectory(outputDirAbsolutePath())
      .debug(true)
      .build();

    test(req, "clustering-server-response-bytes-data-animals-dna.json", "expected-clustering-result-bytes-data-animals-dna.json", false);
  }

  @Test
  public void build_animals_dna_clustering_with_bytes_data_files_in_debug_mode() throws Exception {
    // Given
    BytesData[] bytesData = BytesData.loadFromDirectory(new File(inputDirectory()), File::getName);

    ClusteringRequest req = ClusteringRequest.builder()
      .bytesData(bytesData)
      .outputDirectory(outputDirAbsolutePath())
      .debug(true)
      .build();

    test(req, "clustering-server-response-animals-dna.json", "expected-clustering-result-bytes-data-files-animals-dna.json", true);
  }

  private void test(ClusteringRequest req, String serverResponse, String expectedJson, boolean debug) throws IOException, InterruptedException {
    TaskController taskController = setupTaskController();
    ClusteringController clusteringController = setupClusteringController(url(), taskController);

    setupServer(getClass(), server, serverResponse);

    // when
    RemoteTask remoteTask = clusteringController.clustering(req, new MockHttpServletResponse());

    Clustering clustering;
    do {
      Thread.sleep(300);
      clustering = (Clustering) taskController.task(remoteTask.id, new MockHttpServletResponse());
    } while (clustering == null);

    // then
    if (INTEGRATION_TEST) {
      List<String> outputDirectoryList = Arrays.asList(new File(OUTPUT_DIRECTORY + "/" + remoteTask.id).list());
      if (debug) {
        assertThat(outputDirectoryList).contains("metadata.json");
        assertThat(outputDirectoryList).contains("safe-working-dir");
      }
      assertThat(outputDirectoryList).contains("mediation-request.json.gz");
      assertThat(outputDirectoryList).contains("mediation-graph.json.gz");
      assertThat(outputDirectoryList).contains("mediation-result.json.gz");
    }

    JsonGenericHandler.instance().writeObjectToJsonFile(clustering, new File("fifi"));

    Clustering expected = expectedClusteringResult(expectedJson);
    assertThat(clustering).isEqualTo(expected);

    Clustering readResultOnDisk = readResultOnDisk(remoteTask.id);
    readResultOnDisk = readResultOnDisk.dropGraph();
    assertThat(readResultOnDisk).isEqualTo(expected);
  }

  private String inputDirectory() {
    return new File(getClass().getResource("/animals-dna").getFile()).getAbsolutePath();
  }

  private Clustering expectedClusteringResult(String expectedJson) throws IOException {
    return (Clustering) JsonGenericHandler.instance().getObjectFromJsonFile(Clustering.class, new File(getClass().getResource("/" + expectedJson).getFile()));
  }

  private Clustering readResultOnDisk(String taskId) throws IOException {
    JsonGenericHandler jsonGenericHandler = new JsonGenericHandler();
    Clustering clusteringRequestResult = (Clustering) jsonGenericHandler.getObjectFromJsonGzipFile(
      Clustering.class,
      new File(outputDirForTask(taskId).getAbsolutePath() + "/mediation-result.json.gz")
    );
    return clusteringRequestResult;
  }

  private String outputDirAbsolutePath() {
    return new File(OUTPUT_DIRECTORY).getAbsolutePath();
  }

  private File outputDirForTask(String taskId) {
    return new File(OUTPUT_DIRECTORY + File.separator + taskId);
  }

  static void setupServer(Class clazz, MockWebServer server, String expectedJson) throws IOException {
    if (INTEGRATION_TEST) {
      return;
    }
    String remoteTask = "{\"id\": \"1234\"}";
    server.enqueue((new MockResponse()).setBody(remoteTask));

    File file = new File(clazz.getResource("/" + expectedJson).getFile());
    String json = FileUtils.readFileToString(file, "utf8");
    server.enqueue((new MockResponse()).setBody(json));
  }

  static ClusteringController setupClusteringController(String url, TaskController taskController) {
    ClusteringController clusteringController = new ClusteringController();
    clusteringController.tasks = taskController.getTasks();
    ClusteringServiceImpl clusteringService = new ClusteringServiceImpl();
    AppPrepData appPrepData = new AppPrepData();
    appPrepData.setSharedDirectory(new SharedDirectory());
    clusteringService.appPrepData = appPrepData;
    RemoteClustering remoteClustering = new RemoteClustering();

    remoteClustering.setUrl(url);
    clusteringService.remoteClustering = remoteClustering;
    clusteringController.clusteringService = clusteringService;
    clusteringController.setSharedDirectory(new SharedDirectory());
    return clusteringController;
  }

  static TaskController setupTaskController() {
    TaskController taskController = new TaskController();
    taskController.setTasks(new Tasks());
    return taskController;
  }

  private String url() {
    return INTEGRATION_TEST ? "http://localhost:1958" : server.url("/").toString();
  }
}
