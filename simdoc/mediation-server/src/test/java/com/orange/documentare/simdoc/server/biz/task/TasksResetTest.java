package com.orange.documentare.simdoc.server.biz.task;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.simdoc.server.biz.RemoteTask;
import com.orange.documentare.simdoc.server.biz.distances.DistancesController;
import com.orange.documentare.simdoc.server.biz.distances.DistancesRequest;
import io.vavr.collection.List;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.File;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class TasksResetTest {

  private static final String[] ANIMALS = {
    "pigmyChimpanzee", "chimpanzee", "human"
  };

  @Test
  public void kill_all_tasks() throws Exception {
    // Given
    BytesData element = load("human");
    BytesData[] elements = loadAnimals();

    DistancesRequest req = DistancesRequest.builder()
      .element(new BytesData[] {element})
      .compareTo(elements)
      .build();

    Tasks tasks = new Tasks();
    TaskController taskController = new TaskController();
    taskController.setTasks(tasks);

    DistancesController distancesController = new DistancesController();
    distancesController.setTasks(tasks);

    RemoteTask remoteTask = distancesController.distances(req, new MockHttpServletResponse());

    // when
    taskController.killAll();

    // then
    assertThat(tasks.exists(remoteTask.id)).isFalse();
  }

  private BytesData load(String id) {
    File file = new File(getClass().getResource("/animals-dna/" + id).getFile());
    return new BytesData(List.of(id).toJavaList(), List.of(file.getAbsolutePath()).toJavaList());
  }

  private BytesData[] loadAnimals() {
    return Arrays.stream(ANIMALS)
      .map(this::load)
      .collect(Collectors.toList())
    .toArray(new BytesData[ANIMALS.length]);
  }
}
