package com.orange.documentare.simdoc.server.biz.distances;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.image.opencv.OpencvLoader;
import com.orange.documentare.simdoc.server.biz.RemoteTask;
import com.orange.documentare.simdoc.server.biz.SharedDirectory;
import com.orange.documentare.simdoc.server.biz.distances.remoteclient.RemoteDistance;
import com.orange.documentare.simdoc.server.biz.prepdata.AppPrepData;
import com.orange.documentare.simdoc.server.biz.task.TaskController;
import com.orange.documentare.simdoc.server.biz.task.Tasks;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.apache.commons.io.FileUtils;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.zip.CRC32;

import static com.orange.documentare.simdoc.server.biz.distances.DistancesAnimalsTest.distanceTest;
import static org.assertj.core.api.Assertions.assertThat;

public class DistancesGlyphsTest {

  private static final String[] GLYPHS = {
    "d1.png", "d2.png", "e1.png", "s1.png"
  };
  private static final String GLYPHS_CLUSTERING_SERVER_RESPONSE = "{\n" +
    " \"distances\" : [ 0, 90309, 984556, 997160 ],\n" +
    " \"error\" : false\n" +
    "}";
  private static final int[] GLYPHS_EXPECTED_DISTANCES = {
    0, 90309, 984556, 997160
  };

  @Rule
  public MockWebServer server = new MockWebServer();

  @Test
  @Ignore
  public void compute_glyph_d1_to_glyphs_distances() throws Exception {
    // given
    OpencvLoader.load();

    BytesData[] element = {loadGlyph("d1.png")};
    BytesData[] elements = loadGlyphs();

    DistancesRequest req = DistancesRequest.builder()
      .element(element)
      .compareTo(elements)
      .build();

    server.enqueue(new MockResponse().setBody("{\"id\": \"1234\"}"));
    server.enqueue(new MockResponse().setBody(GLYPHS_CLUSTERING_SERVER_RESPONSE));

    // when
    Tuple2<String, DistancesRequestResult> result = test(req);

    // then
    IntStream.range(0, GLYPHS.length).forEach(i ->
      assertThat(result._2.distances[i]).isEqualTo(GLYPHS_EXPECTED_DISTANCES[i])
    );

    File firstSafeFile = new File("/tmp/distances_prep_dir_1/" + result._1 + "/safe-working-dir/0");
    CRC32 crc32 = new CRC32();
    crc32.update(FileUtils.readFileToByteArray(firstSafeFile));
    // in raw mode
    assertThat(Files.isSymbolicLink(firstSafeFile.toPath())).isFalse();
    assertThat(crc32.getValue()).isEqualTo(1636621735L);
  }


  private Tuple2<String, DistancesRequestResult> test(DistancesRequest req) throws IOException, InterruptedException {
    return distanceTest(req, server.url("/"));
  }

  private BytesData loadGlyph(String id) {
    return load(id, "/glyphs/");
  }

  private BytesData load(String id, String path) {
    File file = new File(getClass().getResource(path + id).getFile());
    return new BytesData(List.of(id).toJavaList(), List.of(file.getAbsolutePath()).toJavaList());
  }

  private BytesData[] loadGlyphs() {
    return Arrays.stream(GLYPHS)
      .map(this::loadGlyph)
      .collect(Collectors.toList())
      .toArray(new BytesData[GLYPHS.length]);
  }
}
