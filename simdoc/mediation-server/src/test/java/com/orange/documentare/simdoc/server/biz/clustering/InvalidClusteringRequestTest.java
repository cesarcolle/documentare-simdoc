package com.orange.documentare.simdoc.server.biz.clustering;

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.simdoc.server.biz.task.TaskController;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.File;

import static com.orange.documentare.simdoc.server.biz.clustering.ClusteringAnimalsTest.setupClusteringController;
import static com.orange.documentare.simdoc.server.biz.clustering.ClusteringAnimalsTest.setupTaskController;
import static org.assertj.core.api.Assertions.assertThat;

public class InvalidClusteringRequestTest {
  private static final String INPUT_DIRECTORY = "in";
  private static final String OUTPUT_DIRECTORY = "out";

  @Before
  public void setup() {
    cleanup();
    new File(OUTPUT_DIRECTORY).mkdir();
  }

  @After
  public void cleanup() {
    FileUtils.deleteQuietly(new File(INPUT_DIRECTORY));
    FileUtils.deleteQuietly(new File(OUTPUT_DIRECTORY));
  }


  @Test
  public void clustering_api_return_bad_request_if_input_directory_and_bytes_data_are_missing() throws Exception {
    // Given
    String expectedMessage = "inputDirectory and bytesData are missing";
    ClusteringRequest req = ClusteringRequest.builder()
      .outputDirectory(OUTPUT_DIRECTORY)
      .build();

    test(req, expectedMessage);
  }

  @Test
  public void clustering_api_return_bad_request_if_input_directory_is_not_reachable() throws Exception {
    // Given
    String expectedMessage = "inputDirectory can not be reached: /xxx";
    ClusteringRequest req = ClusteringRequest.builder()
      .inputDirectory("/xxx")
      .outputDirectory(OUTPUT_DIRECTORY)
      .build();

    test(req, expectedMessage);
  }

  @Test
  public void clustering_api_return_bad_request_if_input_directory_is_not_a_directory() throws Exception {
    // Given
    String expectedMessage = "inputDirectory is not a directory: ";
    FileUtils.writeStringToFile(new File(INPUT_DIRECTORY), "hi");
    ClusteringRequest req = ClusteringRequest.builder()
      .inputDirectory(INPUT_DIRECTORY)
      .outputDirectory(OUTPUT_DIRECTORY)
      .build();

    test(req, expectedMessage);
  }

  @Test
  public void clustering_api_return_bad_request_if_output_directory_is_missing() throws Exception {
    // Given
    String expectedMessage = "outputDirectory is missing";
    createInputDirectory();
    ClusteringRequest req = ClusteringRequest.builder()
      .inputDirectory(INPUT_DIRECTORY)
      .build();

    test(req, expectedMessage);
  }

  @Test
  public void clustering_api_return_bad_request_if_output_directory_is_not_a_directory() throws Exception {
    // Given
    String expectedMessage = "outputDirectory is not a directory: ";
    cleanup();
    createInputDirectory();
    FileUtils.writeStringToFile(new File(OUTPUT_DIRECTORY), "hi");
    ClusteringRequest req = ClusteringRequest.builder()
      .inputDirectory(INPUT_DIRECTORY)
      .outputDirectory(OUTPUT_DIRECTORY)
      .build();

    test(req, expectedMessage);
  }

  @Test
  public void clustering_api_return_bad_request_if_output_directory_is_not_writable() throws Exception {
    // Given
    String expectedMessage = "outputDirectory is not writable:";
    createInputDirectory();
    ClusteringRequest req = ClusteringRequest.builder()
      .inputDirectory(INPUT_DIRECTORY)
      .outputDirectory("/")
      .build();

    test(req, expectedMessage);
  }

  private void test(ClusteringRequest req, String expectedMessage) throws Exception {
    TaskController taskController = setupTaskController();
    ClusteringController clusteringController = setupClusteringController("", taskController);

    // when
    MockHttpServletResponse res = new MockHttpServletResponse();
    clusteringController.clustering(req, res);

    // then
    assertThat(res.getStatus()).isEqualTo(400);
    assertThat(res.getErrorMessage()).contains(expectedMessage);
  }

  private void createInputDirectory() {
    (new File(INPUT_DIRECTORY)).mkdir();
  }
}
