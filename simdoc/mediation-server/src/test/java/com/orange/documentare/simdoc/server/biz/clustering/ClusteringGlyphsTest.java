package com.orange.documentare.simdoc.server.biz.clustering;

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.model.json.JsonGenericHandler;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.simdoc.server.biz.RemoteTask;
import com.orange.documentare.simdoc.server.biz.task.TaskController;
import com.orange.documentare.simdoc.server.biz.task.Tasks;
import okhttp3.mockwebserver.MockWebServer;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.zip.CRC32;

import static com.orange.documentare.simdoc.server.biz.clustering.ClusteringAnimalsTest.setupClusteringController;
import static com.orange.documentare.simdoc.server.biz.clustering.ClusteringAnimalsTest.setupServer;
import static com.orange.documentare.simdoc.server.biz.clustering.ClusteringAnimalsTest.setupTaskController;
import static org.assertj.core.api.Assertions.assertThat;

public class ClusteringGlyphsTest {
  private static final String OUTPUT_DIRECTORY = "out";

  /**
   * set to true to run against a real clustering server
   */
  private static final boolean INTEGRATION_TEST = false;

  @Rule
  public MockWebServer server = new MockWebServer();

  @Before
  public void setup() {
    cleanup();
    new File(OUTPUT_DIRECTORY).mkdir();
  }

  @After
  public void cleanup() {
    FileUtils.deleteQuietly(new File(OUTPUT_DIRECTORY));
  }


  @Test
  public void build_glyphs_clustering_with_input_directory() throws Exception {
    // Given
    ClusteringRequest req = ClusteringRequest.builder()
      .inputDirectory(inputDirectory())
      .outputDirectory(outputDirAbsolutePath())
      .debug(true)
      .build();

    String taskId = test(req, "cluster-server-response-glyphs.json");

    File firstSafeFile = new File(outputDirForTask(taskId).getAbsolutePath() + "/safe-working-dir/0");
    CRC32 crc32 = new CRC32();
    crc32.update(FileUtils.readFileToByteArray(firstSafeFile));

    // in raw mode
    assertThat(Files.isSymbolicLink(firstSafeFile.toPath())).isFalse();
    assertThat(crc32.getValue()).isEqualTo(1636621735L);
  }

  @Test
  public void build_glyphs_clustering_with_bytes_data() throws Exception {
    // Given
    File inputDirectory = new File(inputDirectory());
    BytesData[] bytesData = BytesData.loadFromDirectory(inputDirectory, BytesData.relativePathIdProvider(inputDirectory));
    ClusteringRequest req = ClusteringRequest.builder()
      .bytesData(bytesData)
      .outputDirectory(outputDirAbsolutePath())
      .debug(true)
      .build();

    String taskId = test(req, "cluster-server-response-glyphs.json");

    File firstSafeFile = new File(outputDirForTask(taskId) + "/safe-working-dir/0");
    CRC32 crc32 = new CRC32();
    crc32.update(FileUtils.readFileToByteArray(firstSafeFile));
    // in raw mode
    assertThat(Files.isSymbolicLink(firstSafeFile.toPath())).isFalse();
    assertThat(crc32.getValue()).isEqualTo(1636621735L);
  }


  private String test(ClusteringRequest req, String serverResponse) throws IOException, InterruptedException {
    TaskController taskController = setupTaskController();
    ClusteringController clusteringController = setupClusteringController(url(), taskController);

    setupServer(getClass(), server, serverResponse);

    // when
    RemoteTask remoteTask = clusteringController.clustering(req, new MockHttpServletResponse());

    Clustering clustering;
    do {
      Thread.sleep(300);
      clustering = (Clustering) taskController.task(remoteTask.id, new MockHttpServletResponse());
    } while (clustering == null);

    // then
    JsonGenericHandler.instance().writeObjectToJsonFile(clustering, new File("fifi"));

    Clustering expected = expectedClusteringResult();
    assertThat(clustering).isEqualTo(expected);

    return remoteTask.id;
  }

  private String url() {
    return INTEGRATION_TEST ? "http://localhost:1958" : server.url("/").toString();
  }

  private String inputDirectory() {
    return new File(getClass().getResource("/glyphs").getFile()).getAbsolutePath();
  }

  private Clustering expectedClusteringResult() throws IOException {
    return (Clustering) JsonGenericHandler.instance().getObjectFromJsonFile(Clustering.class,
      new File(getClass().getResource("/expected-clustering-result-glyphs.json").getFile())
    );
  }

  private String outputDirAbsolutePath() {
    return new File(OUTPUT_DIRECTORY).getAbsolutePath();
  }

  private File outputDirForTask(String taskId) {
    return new File(OUTPUT_DIRECTORY + File.separator + taskId);
  }
}
