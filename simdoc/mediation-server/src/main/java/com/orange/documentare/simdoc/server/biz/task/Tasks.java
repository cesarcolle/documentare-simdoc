package com.orange.documentare.simdoc.server.biz.task;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.google.common.annotations.VisibleForTesting;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.Consumer;

@Slf4j
@Component
public class Tasks implements TaskThread.ActiveThreadListener {

  private final Map<String, Task> tasks = new HashMap<>();
  private final int maxTasks = Runtime.getRuntime().availableProcessors();
  private final List<Thread> activeThreads = new ArrayList<>();

  public synchronized Optional<String> run(Consumer<String> taskToRun) {
    if (!canAcceptNewTask()) {
      return Optional.empty();
    }
    String taskId = newTask();
    run(() -> taskToRun.accept(taskId), taskId);
    return Optional.of(taskId);
  }

  public synchronized boolean exists(String id) {
    return tasks.get(id) != null;
  }

  public synchronized boolean isDone(String id) {
    Task task = tasks.get(id);
    if (task == null) {
      log.warn("Invalid task ids: " + id);
      return false;
    }
    return task.result.isPresent();
  }

  public synchronized void addResult(String id, Object result) {
    tasks.put(id, tasks.get(id).withResult(result));
  }

  public synchronized void addErrorResult(String id, Object result) {
    tasks.put(id, tasks.get(id).withErrorResult(result));
  }

  public synchronized Task pop(String id) {
    Task task = tasks.get(id);
    tasks.remove(id);
    return task;
  }

  public synchronized boolean present(String id) {
    return tasks.containsKey(id);
  }

  @Override
  public synchronized void finished(Thread thread) {
    activeThreads.remove(thread);
  }

  @Override
  public synchronized void error(Thread thread, String taskId) {
    // remove silently
    tasks.remove(taskId);
    // FIXME: test me please
    activeThreads.remove(thread);
  }

  boolean canAcceptNewTask() {
    return activeThreads.size() < maxTasks;
  }

  @VisibleForTesting
  String newTask() {
    Task task = new Task();
    tasks.put(task.id, task);
    return task.id;
  }

  void run(Runnable runnable, String taskId) {
    Thread thread = new TaskThread(runnable, this, taskId);
    activeThreads.add(thread);
    thread.start();
  }

  public synchronized void killAll() {
    tasks.clear();
    activeThreads.forEach(thread -> {
      try {
        thread.stop();
      } catch (ThreadDeath e) {
        // catch silently
      }
    });
    activeThreads.clear();
  }
}
