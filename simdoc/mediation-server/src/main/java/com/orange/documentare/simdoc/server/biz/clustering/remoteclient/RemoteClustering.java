package com.orange.documentare.simdoc.server.biz.clustering.remoteclient;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */


import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.simdoc.server.biz.RemoteTask;
import com.orange.documentare.simdoc.server.biz.clustering.ClusteringRequest;
import com.orange.documentare.simdoc.server.biz.task.WaitingPeriod;
import feign.Feign;
import feign.FeignException;
import feign.Response;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
@Service
public class RemoteClustering {

  @Setter
  @Value("${clustering.server.url}")
  private String url;

  public Clustering request(ClusteringRequest clusteringRequest) {
    ClusteringApi api  = buildFeignRequest();
    try {
      return doRequest(api, clusteringRequest);
    } catch (FeignException |IOException |InterruptedException e) {
      handleError(e);
    }
    return null;
  }

  private Clustering doRequest(ClusteringApi api, ClusteringRequest clusteringRequest) throws IOException, InterruptedException {

    RemoteTask remoteTask = api.clusteringApi(clusteringRequest);
    log.info("[REQUEST POSTED] {} {}", remoteTask.id, url);
    return waitForResult(remoteTask.id, url);
  }

  private void handleError(Exception e) {
    if (e instanceof FeignException) {
      int status = ((FeignException) e).status();
      if (status == 503) {
        log.info("Service {} can not handle more tasks", url, status, e.getMessage());
      } else {
        log.error("Request to {} failed with status {}: {}", url, status, e.getMessage());
      }
    } else {
      log.error("Request to {} failed: {}", url, e.getMessage());
    }
  }
  private ClusteringApi buildFeignRequest() {
    return Feign.builder()
            .encoder(new JacksonEncoder())
            .decoder(new JacksonDecoder())
           .target(ClusteringApi.class, url);
  }

  private Clustering waitForResult(String taskId, String url) throws IOException, InterruptedException {
    WaitingPeriod waitingPeriod = new WaitingPeriod();
    ClusteringResultApi clusteringResult = Feign.builder()
            .encoder(new JacksonEncoder())
            .decoder(new JacksonDecoder())
            .target(ClusteringResultApi.class, url);
    Response response;
    do {
      waitingPeriod.sleep();
      response = clusteringResult.clusteringResultApi(taskId);
    } while (response.status() == 204);
    log.info("[RESULT RECEIVED] {}", taskId);
    return (Clustering) (new JacksonDecoder())
            .decode(response, Clustering.class);
  }
}
