package com.orange.documentare.simdoc.server.biz.distances;

import com.orange.documentare.simdoc.server.biz.RemoteTask;
import com.orange.documentare.simdoc.server.biz.clustering.RequestValidation;
import com.orange.documentare.simdoc.server.biz.distances.remoteclient.DistancesSegment;
import com.orange.documentare.simdoc.server.biz.distances.remoteclient.RemoteDistance;
import com.orange.documentare.simdoc.server.biz.prepdata.AppPrepData;
import com.orange.documentare.simdoc.server.biz.task.Tasks;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_SERVICE_UNAVAILABLE;

@Slf4j
@RestController
public class DistancesController implements DistancesApi {

  @RequiredArgsConstructor
  class ComputeDistance {
    private final DistancesRequest req;

    public void run(String taskId) {
      Try.of(() -> compute(req, taskId))
        .onSuccess(result -> {
          tasks.addResult(taskId, result);
          appPrepData.removeSafeWorkingDirectoryForDistance(taskId);
        })
        .onFailure(throwable -> tasks.addErrorResult(taskId, DistancesRequestResult.error(throwable.getMessage())));
    }

    private DistancesRequestResult compute(DistancesRequest req, String taskId) throws IOException, InterruptedException {
      DistancesSegment distancesSegment = appPrepData.createSafeWorkingDirectoryForDistances(req, taskId);
      distancesSegment = remoteDistance.request(distancesSegment);
      int[] remappedDistances = appPrepData.remapDistances(req, distancesSegment.elements, distancesSegment.distances, taskId);

      return DistancesRequestResult.with(remappedDistances);
    }
  }


  @Setter
  @Autowired
  Tasks tasks;

  @Autowired
  RemoteDistance remoteDistance;

  @Autowired
  AppPrepData appPrepData;

  @Override
  public RemoteTask distances(
    @RequestBody DistancesRequest req, HttpServletResponse res) throws IOException {
    log.info("[DISTANCES REQ] for element ids: " + req.element[0].ids);

    RequestValidation validation = req.validate();
    if (!validation.ok) {
      res.sendError(SC_BAD_REQUEST, validation.error);
      return new RemoteTask();
    }

    ComputeDistance computeDistance = new ComputeDistance(req);
    Optional<String> taskId = tasks.run(tId -> computeDistance.run(tId));

    if (!taskId.isPresent()) {
      res.sendError(SC_SERVICE_UNAVAILABLE, "can not accept more tasks");
      return new RemoteTask();
    }

    return new RemoteTask(taskId.get());
  }
}
