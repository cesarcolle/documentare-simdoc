package com.orange.documentare.simdoc.server.biz.clustering;

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.model.json.JsonGenericHandler;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.simdoc.server.biz.FileIO;
import com.orange.documentare.simdoc.server.biz.clustering.remoteclient.RemoteClustering;
import com.orange.documentare.simdoc.server.biz.prepdata.AppPrepData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

@Service
public class ClusteringServiceImpl implements ClusteringService {

  static {
    //OpencvLoader.load();

    // use rather this line if you want to debug in intellij, since for an unknown reason intellij can not use
    // system libraries... (/usr/lib/jni)
    nu.pattern.OpenCV.loadShared();
  }

  @Autowired
  AppPrepData appPrepData;

  @Autowired
  RemoteClustering remoteClustering;

  @Override
  public Clustering build(String taskId, FileIO fileIO, ClusteringRequest clusteringRequest) throws IOException {
    // Raw result on prepped files
    Clustering clustering = buildRemoteClustering(taskId, clusteringRequest);

    JsonGenericHandler.instance().writeObjectToJsonFile(clustering, new File("titiittiti"));

    // Remap result
    clustering = clusteringRequest.bytesDataMode() ?
      ClusteringRemap.remapInBytesDataMode(clusteringRequest.bytesData, clustering, fileIO) :
      ClusteringRemap.remapInFilesMode(fileIO, clustering);

    fileIO.writeClusteringRequestResult(clustering);
    if (!clusteringRequest.debug()) {
      fileIO.cleanupClustering();
    }

    clustering = clustering.dropGraph();
    return clustering;
  }

  private Clustering buildRemoteClustering(String taskId, ClusteringRequest mediationRequest) throws IOException {
    BytesData[] bytesDataArray = prepData(taskId, mediationRequest);
    Clustering clustering = remoteClustering.request(
      ClusteringRequest.forClusteringServer(bytesDataArray, mediationRequest, taskId));
    return clustering;
  }

  private BytesData[] prepData(String taskId, ClusteringRequest req) {
    // if bytes are already loaded, there is no directory to prep
    boolean nothingToPrep = req.bytesDataMode() && req.bytesData[0].bytes != null;
    if (nothingToPrep) {
      return req.bytesData;
    }
    return appPrepData.createSafeWorkingDirectoryForClustering(taskId, req);
  }
}
