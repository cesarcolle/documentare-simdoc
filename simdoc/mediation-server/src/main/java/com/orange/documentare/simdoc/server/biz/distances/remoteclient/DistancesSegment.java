package com.orange.documentare.simdoc.server.biz.distances.remoteclient;

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;

public class DistancesSegment {
  /** element which will be compared (distance) to ... */
  public final BytesData element;
  /** ... compared (distance) to these elements */
  public final BytesData[] elements;
  public final int[] distances;

  public DistancesSegment withDistances(int[] computedDistances) {
    return new DistancesSegment(element, elements, computedDistances);
  }

  public DistancesSegment(BytesData element, BytesData[] elements) {
    this(element, elements, null);
  }

  private DistancesSegment(BytesData element, BytesData[] elements, int[] distances) {
    this.element = element;
    this.elements = elements;
    this.distances = distances;
  }
}
