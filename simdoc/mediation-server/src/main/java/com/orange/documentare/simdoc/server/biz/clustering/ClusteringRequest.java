package com.orange.documentare.simdoc.server.biz.clustering;

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters;
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters.ClusteringParametersBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.io.File;

@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
@Builder
public class ClusteringRequest {
  @ApiModelProperty(example = "myDocuments")
  public final String inputDirectory;
  @ApiModelProperty(example = "{[{'ids':'titi', 'filepaths':'/home/titi', 'bytes':[0x1,0x3...]},...])")
  public final BytesData[] bytesData;
  @ApiModelProperty(example = "clusteringOutput", required = true)
  public final String outputDirectory;
  @ApiModelProperty(example = "1048576")
  public final Integer pCount;
  @ApiModelProperty(example = "true")
  public final Boolean debug;
  @ApiModelProperty(example = "2")
  public final Float acutSdFactor;
  @ApiModelProperty(example = "2")
  public final Float qcutSdFactor;
  @ApiModelProperty(example = "2")
  public final Float scutSdFactor;
  @ApiModelProperty(example = "75")
  public final Integer ccutPercentile;
  @ApiModelProperty(example = "false")
  public final Boolean wcut;
  @ApiModelProperty(example = "5")
  public final Integer kNearestNeighboursThreshold;
  @ApiModelProperty(example = "false")
  public final Boolean sloop;
  @ApiModelProperty(example = "false")
  public final Boolean allInSameCluster;
  @ApiModelProperty(example = "false")
  public final Boolean enroll;

  public RequestValidation validate() {
    boolean valid = false;
    String error = null;
    if (inputDirectory == null && bytesData == null) {
      error = "inputDirectory and bytesData are missing";
    } else if (outputDirectory == null) {
      error = "outputDirectory is missing";
    } else {
      valid = true;
    }
    return new RequestValidation(valid, error);
  }

  public ClusteringParameters clusteringParameters() {
    ClusteringParametersBuilder builder = ClusteringParameters.builder();
    if (acutSdFactor != null) {
      builder.acut(acutSdFactor);
    }
    if (qcutSdFactor != null) {
      builder.qcut(qcutSdFactor);
    }
    if (scutSdFactor != null) {
      builder.scut(scutSdFactor);
    }
    if (ccutPercentile != null) {
      builder.ccut(ccutPercentile);
    }
    if (wcut != null && wcut) {
      builder.wcut();
    }
    if (kNearestNeighboursThreshold!= null) {
      builder.knn(kNearestNeighboursThreshold);
    }
    if (sloop != null && sloop) {
      builder.sloop();
    }
    if (allInSameCluster != null && allInSameCluster) {
      builder.allInSameCluster();
    }
    if (enroll != null && enroll) {
      builder.enroll();
    }
    return builder.build();
  }

  /** can not be a computed field, since json deserialization is not using the ctor */
  public boolean bytesDataMode() {
    return bytesData != null;
  }

  public boolean debug() {
    return debug != null && debug;
  }

  public static ClusteringRequest forClusteringServer(BytesData[] bytesData, ClusteringRequest r, String taskId) {
    return builder()
      .bytesData(bytesData)
      .outputDirectory(r.outputDirectory + File.separator + taskId)
      //.pCount(r.pCount)
      .debug(r.debug)
      .acutSdFactor(r.acutSdFactor)
      .qcutSdFactor(r.qcutSdFactor)
      .scutSdFactor(r.scutSdFactor)
      .ccutPercentile(r.ccutPercentile)
      .wcut(r.wcut)
      .kNearestNeighboursThreshold(r.kNearestNeighboursThreshold)
      .sloop(r.sloop)
      .allInSameCluster(r.allInSameCluster)
      .enroll(r.enroll)
      .build();
    }
}
