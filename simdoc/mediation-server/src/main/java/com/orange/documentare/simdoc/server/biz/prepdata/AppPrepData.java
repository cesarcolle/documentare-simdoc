package com.orange.documentare.simdoc.server.biz.prepdata;

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.system.inputfilesconverter.FilesMap;
import com.orange.documentare.simdoc.server.biz.FileIO;
import com.orange.documentare.simdoc.server.biz.SharedDirectory;
import com.orange.documentare.simdoc.server.biz.clustering.ClusteringRequest;
import com.orange.documentare.simdoc.server.biz.distances.DistancesRequest;
import com.orange.documentare.simdoc.server.biz.distances.remoteclient.DistancesSegment;
import lombok.Setter;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Arrays;
import java.util.stream.IntStream;

@Component
public class AppPrepData {

  private static final String DISTANCE_PREP_DIR = "/tmp/distances_prep_dir_";

  @Setter
  @Autowired
  SharedDirectory sharedDirectory;

  public DistancesSegment createSafeWorkingDirectoryForDistances(DistancesRequest req, String taskId) {

    BytesData[] elementAsArray = req.element;
    FileIO fileIO1 = new FileIO(sharedDirectory, elementAsArray, DISTANCE_PREP_DIR + "1").forTaskId(taskId);
    FileIO fileIO2 = new FileIO(sharedDirectory, req.elements, DISTANCE_PREP_DIR + "2").forTaskId(taskId);

    BytesData[] elementBytesData = createSafeWorkingDirectory(fileIO1, elementAsArray, req.pCount);
    BytesData[] elements = createSafeWorkingDirectory(fileIO2, req.elements, req.pCount);

    return new DistancesSegment(elementBytesData[0], elements);
  }

  public BytesData[] createSafeWorkingDirectoryForClustering(String taskId, ClusteringRequest req) {
    FileIO fileIO = (new FileIO(sharedDirectory, req)).forTaskId(taskId);
    return createSafeWorkingDirectory(fileIO, req.bytesData, req.pCount);
  }

  public BytesData[] createSafeWorkingDirectory(FileIO fileIO, BytesData[] bytesData, Integer pCount) {
    int expectedPixelsCount = pCount == null ? 1024*1024 : pCount;

    com.orange.documentare.core.prepdata.PrepData prepData = com.orange.documentare.core.prepdata.PrepData.builder()
      .inputDirectory(fileIO.inputDirectory()) // may be null
      .bytesData(bytesData) // may be null
      .withRawConverter(true)
      .expectedRawPixelsCount(expectedPixelsCount)
      .safeWorkingDirConverter()
      .safeWorkingDirectory(fileIO.safeWorkingDirectory())
      .metadataOutputFile(fileIO.metadataFile())
      .build();
    prepData.prep();

    File safeWorkingDirectory = fileIO.safeWorkingDirectory();

    BytesData[] bytesData1 = BytesData.loadFromDirectory(
      safeWorkingDirectory, BytesData.relativePathIdProvider(safeWorkingDirectory)
    );
    return bytesData1;
  }

  public int[] remapDistances(DistancesRequest req, BytesData[] safeElements, int[] distances, String taskId) {
    FileIO fileIO = new FileIO(sharedDirectory, req.elements, DISTANCE_PREP_DIR + "2").forTaskId(taskId);
    FilesMap filesMap = fileIO.loadFilesMap();

    int[] remappedDistances = new int[distances.length];
    IntStream.range(0, distances.length).forEach(i -> {
      BytesData bd = req.elements[i];
      int fileId = filesMap.getKeyByValue(bd.filepaths.get(0)).get();
      BytesData element = lookUpElement(safeElements, fileId);
      int index = Arrays.asList(safeElements).indexOf(element);
      remappedDistances[i] = distances[index];
    });
    return remappedDistances;
  }

  private BytesData lookUpElement(BytesData[] elements, int fileId) {
    return Arrays.stream(elements)
      .filter(element -> Integer.parseInt(element.ids.get(0)) == fileId)
      .findFirst()
      .get();
  }

  public void removeSafeWorkingDirectoryForDistance(String taskId) {
    String distancePrepDir1 = DISTANCE_PREP_DIR + "1" + File.separator + taskId;
    String distancePrepDir2 = DISTANCE_PREP_DIR + "2" + File.separator + taskId;

    FileUtils.deleteQuietly(new File(distancePrepDir1));
    FileUtils.deleteQuietly(new File(distancePrepDir2));

    return;
  }
}
