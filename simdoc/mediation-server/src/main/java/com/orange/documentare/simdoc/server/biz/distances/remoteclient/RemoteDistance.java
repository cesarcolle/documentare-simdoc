package com.orange.documentare.simdoc.server.biz.distances.remoteclient;

import com.orange.documentare.simdoc.server.biz.RemoteTask;
import com.orange.documentare.simdoc.server.biz.distances.DistancesRequestResult;
import com.orange.documentare.simdoc.server.biz.task.WaitingPeriod;
import feign.Feign;
import feign.Response;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
@Service
public class RemoteDistance {

  @Setter
  @Value("${clustering.server.url}")
  private String clusteringServerUrl;

  public DistancesSegment request(DistancesSegment segment) throws IOException, InterruptedException {
    RemoteTask remoteTask = buildFeignRequest().distance(segment);
    log.info("[REQUEST POSTED] {} {}", remoteTask.id, clusteringServerUrl);
    DistancesRequestResult result = waitForResult(remoteTask.id, clusteringServerUrl);
    return segment.withDistances(result.distances);
  }

  private DistanceApi buildFeignRequest() {
    return Feign.builder()
      .encoder(new JacksonEncoder())
      .decoder(new JacksonDecoder())
      .target(DistanceApi.class, clusteringServerUrl);
  }

  private DistancesRequestResult waitForResult(String taskId, String remoteServiceUrl) throws IOException, InterruptedException {
    WaitingPeriod waitingPeriod = new WaitingPeriod();
    DistanceResultApi distanceResult = Feign.builder()
      .encoder(new JacksonEncoder())
      .decoder(new JacksonDecoder())
      .target(DistanceResultApi.class, remoteServiceUrl);
    Response response;
    do {
      waitingPeriod.sleep();
      response = distanceResult.distanceResult(taskId);
    } while (response.status() == 204);

    log.info("[RESULT RECEIVED] {}", taskId);
    return (DistancesRequestResult) (new JacksonDecoder())
      .decode(response, DistancesRequestResult.class);
  }
}
