package com.orange.documentare.simdoc.server.biz.clustering.remoteclient;

import feign.Param;
import feign.RequestLine;
import feign.Response;

interface ClusteringResultApi {
  @RequestLine("GET /task/{taskId}")
  Response clusteringResultApi(@Param("taskId") String taskId);
}
