package com.orange.documentare.simdoc.server.biz.distances.remoteclient;

import com.orange.documentare.simdoc.server.biz.RemoteTask;
import feign.Headers;
import feign.RequestLine;

interface DistanceApi {
  @RequestLine("POST /distances")
  @Headers("Content-Type: application/json")
  RemoteTask distance(DistancesSegment segment);
}
