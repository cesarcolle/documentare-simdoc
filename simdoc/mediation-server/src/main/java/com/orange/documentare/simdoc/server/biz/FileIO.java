package com.orange.documentare.simdoc.server.biz;

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.orange.documentare.core.comp.clustering.graph.GraphvizPath;
import com.orange.documentare.core.comp.clustering.graph.jgrapht.InternalJGraphTBuilder;
import com.orange.documentare.core.comp.clustering.graph.jgrapht.JGraphEdge;
import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.model.json.JsonGenericHandler;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.core.model.ref.clustering.graph.ClusteringGraph;
import com.orange.documentare.core.model.ref.clustering.graph.GraphItem;
import com.orange.documentare.core.system.inputfilesconverter.Metadata;
import com.orange.documentare.core.system.inputfilesconverter.FilesMap;
import com.orange.documentare.core.system.nativeinterface.NativeInterface;
import com.orange.documentare.simdoc.server.biz.clustering.ClusteringRequest;
import com.orange.documentare.simdoc.server.biz.clustering.RequestValidation;
import com.orange.documentare.simdoc.server.biz.clustering.graph.*;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.jgrapht.ext.DOTExporter;
import org.jgrapht.ext.VertexNameProvider;
import org.jgrapht.graph.AbstractBaseGraph;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Optional;

@Slf4j
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class FileIO {
  private static final String DOT_OUTPUT = "graph.dot";

  private static final String SAFE_WORKING_DIR = "/safe-working-dir";
  private static final String CLUSTERING_REQUEST_FILE = "/mediation-request.json.gz";
  private static final String CLUSTERING_RESULT_FILE = "/mediation-result.json.gz";
  private static final String CLUSTERING_GRAPH_FILE = "/mediation-graph.json.gz";
  private static final String METADATA_JSON = "metadata.json";

  public final String inputDirectoryAbsPath;
  public final String outputDirectoryAbsPathLessTaskId;
  public final String outputDirectoryAbsPath;
  private final boolean bytesDataMode;
  private final boolean filesPrepped;

  public FileIO(SharedDirectory sharedDirectory, ClusteringRequest req) {
    this(sharedDirectory, req.bytesDataMode(), req.bytesData, req.inputDirectory, req.outputDirectory, -1);
  }

  public FileIO(SharedDirectory sharedDirectory, BytesData[] bytesData, String outputDirectory) {
    this(sharedDirectory, true, bytesData, null, outputDirectory, -1);
  }

  public FileIO forTaskId(String taskId) {
    String outputDirectoryAbsPath = outputDirectoryAbsPathLessTaskId + File.separator + taskId;
    (new File(outputDirectoryAbsPath)).mkdir();
    return new FileIO(inputDirectoryAbsPath, outputDirectoryAbsPathLessTaskId, outputDirectoryAbsPath, bytesDataMode, filesPrepped);
  }

  private FileIO(SharedDirectory sharedDirectory, boolean bytesDataMode, BytesData[] bytesData, String inputDirectory, String outputDirectory, int taskId) {
    this.bytesDataMode = bytesDataMode;

    // false only if no files preparation is done, ie for bytes data mode when bytes are present in the request
    this.filesPrepped = !bytesDataMode || (bytesDataMode && bytesData.length > 0 && bytesData[0].bytes == null);

    String prefix = sharedDirectory.sharedDirectoryAvailable() ?
      sharedDirectory.sharedDirectoryRootPath() :
      "";

    inputDirectoryAbsPath = inputDirectory == null ? null : new File(prefix + inputDirectory).getAbsolutePath();
    outputDirectoryAbsPathLessTaskId = outputDirectory == null ? null : new File(prefix + outputDirectory).getAbsolutePath();
    outputDirectoryAbsPath = null;
  }

  public RequestValidation validate() {
    boolean valid = false;
    String error = null;
    File outDir = outputDirectoryAbsPathLessTaskId != null ? new File(outputDirectoryAbsPathLessTaskId) : null;

    if (!bytesDataMode && !inputDirectory().exists()) {
      error = "inputDirectory can not be reached: " + inputDirectoryAbsPath;
    } else if (!bytesDataMode && !inputDirectory().isDirectory()) {
      error = "inputDirectory is not a directory: " + inputDirectoryAbsPath;
    } else if (outDir == null || !outDir.exists()) {
      error = "outputDirectory can not be reached: " + outputDirectoryAbsPathLessTaskId;
    } else if (!outDir.isDirectory()) {
      error = "outputDirectory is not a directory: " + outputDirectoryAbsPathLessTaskId;
    } else if (!outDir.canWrite()) {
      error = "outputDirectory is not writable: " + outputDirectoryAbsPathLessTaskId;
    } else {
      valid = true;
    }
    return new RequestValidation(valid, error);
  }

  public void writeClusteringRequestResult(Clustering clustering) throws IOException {
    writeOnDisk(clustering, clusteringResultFile());
    writeClusteringGraph(clustering.graph);
  }

  private void writeClusteringGraph(ClusteringGraph graph) throws IOException {
    if (graph == null) {
      log.warn("Graph is not provided in clustering result");
      return;
    }

    writeOnDisk(graph, clusteringGraphFile());

    if (filesPrepped) {
      Optional<File> imageDirectory = doBuildThumbnails();
      AbstractBaseGraph<GraphItem, JGraphEdge> jgraph = getJGraphTGraph(graph);
      File dotOutput = new File(outputDirectory() + "/" + DOT_OUTPUT);
      export(jgraph, dotOutput, imageDirectory, metadataFile(), graph);
    }
  }

  public void writeRequest(ClusteringRequest req) throws IOException {
    writeOnDisk(req, clusteringRequestFile());
  }

  public void deleteAllClusteringFiles() {
    cleanupClustering();
    FileUtils.deleteQuietly(clusteringRequestFile());
    FileUtils.deleteQuietly(clusteringResultFile());
    FileUtils.deleteQuietly(clusteringGraphFile());
  }

  public void cleanupClustering() {
    FileUtils.deleteQuietly(safeWorkingDirectory());
    FileUtils.deleteQuietly(metadataFile());
  }

  public FilesMap loadFilesMap() {
    JsonGenericHandler jsonGenericHandler = new JsonGenericHandler();
    try {
      return ((Metadata)jsonGenericHandler.getObjectFromJsonFile(Metadata.class, metadataFile()))
        .filesMap;
    } catch (IOException e) {
      throw new IllegalStateException(String.format("Failed to load metadata json '%s': %s", metadataFile().getAbsolutePath(), e.getMessage()));
    }
  }

  public File safeWorkingDirectory() {
    return new File(outputDirectoryAbsPath + SAFE_WORKING_DIR);
  }

  public File inputDirectory() {
    return inputDirectoryAbsPath == null ? null : new File(inputDirectoryAbsPath);
  }

  public File outputDirectory() {
    return new File(outputDirectoryAbsPath);
  }

  private void writeOnDisk(Object o, File file) throws IOException {
    JsonGenericHandler jsonGenericHandler = new JsonGenericHandler(true);
    jsonGenericHandler.getMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);
    jsonGenericHandler.writeObjectToJsonGzipFile(o, file);
  }

  public File metadataFile() {
    return new File(outputDirectoryAbsPath + "/" + METADATA_JSON);
  }

  private File clusteringRequestFile() {
    return new File(outputDirectoryAbsPath + CLUSTERING_REQUEST_FILE);
  }

  public File clusteringGraphFile() {
    return new File(outputDirectoryAbsPath + CLUSTERING_GRAPH_FILE);
  }

  private File clusteringResultFile() {
    return new File(outputDirectoryAbsPath + CLUSTERING_RESULT_FILE);
  }

  public boolean filesPrepped() {
    return filesPrepped;
  }


  private Optional<File>  doBuildThumbnails() throws IOException {
    JsonGenericHandler jsonGenericHandler = new JsonGenericHandler();
    Metadata metadata = (Metadata) jsonGenericHandler.getObjectFromJsonFile(Metadata.class, metadataFile());
    ThumbnailsBuilder thumbnailsBuilder = new ThumbnailsBuilder(outputDirectoryAbsPath, metadata, Optional.empty(), metadata.inputDirectoryPath);
    return thumbnailsBuilder.build();
  }

  private static AbstractBaseGraph<GraphItem, JGraphEdge> getJGraphTGraph(ClusteringGraph clusteringGraph) {
    InternalJGraphTBuilder jGraphTBuilder = new InternalJGraphTBuilder();
    return jGraphTBuilder.getJGraphTFrom(clusteringGraph);
  }

  private static void export(AbstractBaseGraph<GraphItem, JGraphEdge> graph, File dotOutput, Optional<File> imageDirectory, File metadataFile, ClusteringGraph clusteringGraph) throws IOException {
    DOTExporter exporter = new DOTExporter(new IdProvider(), labelProvider(metadataFile), new EdgeLabelProvider(), new VertexAttributeProvider(imageDirectory, clusteringGraph), null);
    FileWriter writer = new FileWriter(dotOutput);
    exporter.exportGraph(graph, writer);
  }

  private static VertexNameProvider labelProvider(File metadataFile) throws IOException {
    JsonGenericHandler jsonGenericHandler = new JsonGenericHandler();
    FilesMap filesMap = ((Metadata)jsonGenericHandler.getObjectFromJsonFile(Metadata.class, metadataFile)).filesMap;
    return new LabelProvider(filesMap);
  }
}
