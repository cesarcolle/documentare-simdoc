package com.orange.documentare.simdoc.server.biz.clustering;

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.distance.bytesdistances.BytesData;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.core.model.ref.clustering.ClusteringElement;
import com.orange.documentare.core.system.inputfilesconverter.FilesMap;
import com.orange.documentare.simdoc.server.biz.FileIO;

import java.io.File;
import java.util.*;
import java.util.stream.IntStream;

public class ClusteringRemap {

  static Clustering remapInFilesMode(FileIO fileIO, Clustering cl) {
    FilesMap map = fileIO.loadFilesMap();

    List<ClusteringElement> remappedElements = new ArrayList<>();

    cl.elements.forEach(el -> {
      int fileId = Integer.valueOf(el.id);
      String fileAbsPath = map.get(fileId);
      /* +1 to remove leading '/' */
      String relPath = fileAbsPath.substring(fileIO.inputDirectoryAbsPath.length() + 1);
      ClusteringElement remappedElement  = new ClusteringElement(
        relPath, el.clusterId, el.clusterCenter, el.enrolled, el.duplicateOf, el.distanceToCenter
      );
      remappedElements.add(remappedElement);
    });

    return cl.updateElementsId(remappedElements);
  }

  static Clustering remapInBytesDataMode(BytesData[] bytesData, Clustering clustering, FileIO fileIO) {
    return fileIO.filesPrepped() ?
        remapInBytesDataModeWithFilesPreparation(bytesData, clustering, fileIO) :
      remapInBytesDataMode(bytesData, clustering);
  }

  private static Clustering remapInBytesDataModeWithFilesPreparation(BytesData[] bytesData, Clustering clustering, FileIO fileIO) {
    FilesMap filesMap = fileIO.loadFilesMap();

    List<ClusteringElement> remappedElements = new ArrayList<>();
    IntStream.range(0, bytesData.length).forEach(i -> {
      BytesData bd = bytesData[i];
      // Normalize filePath : replace // by /
      File file = new File(bd.filepaths.get(0));
      int fileId = filesMap.getKeyByValue(file.getAbsolutePath()).get();
      ClusteringElement element = lookUpElement(clustering.elements, fileId);
      ClusteringElement remappedElement = new ClusteringElement(bd.ids.get(0), element.clusterId, element.clusterCenter, element.enrolled, element.duplicateOf, element.distanceToCenter);
      remappedElements.add(remappedElement);
    });
    return clustering.updateElementsId(remappedElements);
  }

  private static ClusteringElement lookUpElement(List<ClusteringElement> elements, int fileId) {
    return elements.stream()
      .filter(element -> Integer.parseInt(element.id) == fileId)
      .findFirst()
      .get();
  }

  private static Clustering remapInBytesDataMode(BytesData[] bytesData, Clustering clustering) {
    List<ClusteringElement> remappedElements = new ArrayList<>();
    IntStream.range(0, bytesData.length).forEach(i -> {
      ClusteringElement el = clustering.elements.get(i);
      remappedElements.add(new ClusteringElement(bytesData[i].ids.get(0), el.clusterId, el.clusterCenter, el.enrolled, el.duplicateOf, el.distanceToCenter));
    });
    return clustering.updateElementsId(remappedElements);
  }
}
