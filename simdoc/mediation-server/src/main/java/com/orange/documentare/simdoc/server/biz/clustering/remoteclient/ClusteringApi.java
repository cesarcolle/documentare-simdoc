package com.orange.documentare.simdoc.server.biz.clustering.remoteclient;

import com.orange.documentare.simdoc.server.biz.RemoteTask;
import com.orange.documentare.simdoc.server.biz.clustering.ClusteringRequest;
import feign.Headers;
import feign.RequestLine;

interface ClusteringApi {
  @RequestLine("POST /clustering")
  @Headers("Content-Type: application/json")
  RemoteTask clusteringApi(ClusteringRequest clusteringRequest);
}
