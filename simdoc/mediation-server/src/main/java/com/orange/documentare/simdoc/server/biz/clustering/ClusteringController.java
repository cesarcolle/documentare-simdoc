package com.orange.documentare.simdoc.server.biz.clustering;

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.simdoc.server.biz.CachesStats;
import com.orange.documentare.simdoc.server.biz.FileIO;
import com.orange.documentare.simdoc.server.biz.RemoteTask;
import com.orange.documentare.simdoc.server.biz.SharedDirectory;
import com.orange.documentare.simdoc.server.biz.distances.DistancesController;
import com.orange.documentare.simdoc.server.biz.task.Tasks;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_SERVICE_UNAVAILABLE;

@Slf4j
@RestController
public class ClusteringController implements ClusteringApi {

  @RequiredArgsConstructor
  class ComputeClustering {
    private final ClusteringRequest req;
    private final FileIO fileIO;

    void run(String taskId) {
      Try.of(() -> doClustering(taskId, fileIO.forTaskId(taskId), req))
        .onSuccess(result -> {
          if (result != null) {
            tasks.addResult(taskId, result);
          } else {
            tasks.addErrorResult(taskId, Clustering.error(req.clusteringParameters(), "Result is null"));
          }
          CachesStats.log();
        })
        .onFailure(throwable -> {
          Clustering result = Clustering.error(req.clusteringParameters(), throwable.getMessage());
          tasks.addErrorResult(taskId, result);
        });
    }

    private Clustering doClustering(String taskId, FileIO fileIO, ClusteringRequest req) throws IOException {
      fileIO.deleteAllClusteringFiles();
      fileIO.writeRequest(req);
      return clusteringService.build(taskId, fileIO, req);
    }
  }

  @Autowired
  ClusteringService clusteringService;

  @Setter
  @Autowired
  SharedDirectory sharedDirectory;

  @Autowired
  Tasks tasks;


  @Override
  public RemoteTask clustering(
    @RequestBody ClusteringRequest req, HttpServletResponse res) throws IOException {
    log.info("[Clustering request] " + req);

    RequestValidation validation = req.validate();
    if (!validation.ok) {
      res.sendError(SC_BAD_REQUEST, validation.error);
      return new RemoteTask();
    }

    FileIO fileIO = new FileIO(sharedDirectory, req);
    validation = fileIO.validate();
    if (!validation.ok) {
      res.sendError(SC_BAD_REQUEST, validation.error);
      return new RemoteTask();
    }

    ComputeClustering computeClustering = new ComputeClustering(req, fileIO);
    Optional<String> taskId = tasks.run(tId -> computeClustering.run(tId));

    if (!taskId.isPresent()) {
      res.sendError(SC_SERVICE_UNAVAILABLE, "can not accept more tasks");
      return new RemoteTask();
    }

    return new RemoteTask(taskId.get());
  }
}
