package com.orange.documentare.simdoc.server.biz.distances.remoteclient;

import feign.Param;
import feign.RequestLine;
import feign.Response;

interface DistanceResultApi {
  @RequestLine("GET /task/{taskId}")
  Response distanceResult(@Param("taskId") String taskId);
}
