package com.orange.documentare.core.comp.clustering.graph.jgrapht;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.graph.ClusteringGraph;
import com.orange.documentare.core.model.ref.clustering.graph.GraphItem;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor(suppressConstructorProperties = true)
public class AllInSameClusterGraphBuilder extends InternalGraphBuilder {

  @Override
  protected void addEdges(ClusteringGraph clusteringGraph) {
    getGraphItems().stream().forEach(graphItem -> addEdgesFor(graphItem));
  }

  private void addEdgesFor(GraphItem graphItem) {
    int indexVertex1 = graphItem.getVertex1Index();
    int indexVertex2 = graphItem.getVertex2Index();
    int indexVertex3 = graphItem.getVertex3Index();
    int[] edgesLength = graphItem.getEdgesLength();
    int length = edgesLength == null ? 0 : edgesLength.length;
    if (length > 0) {
      tryToConnect(indexVertex1, indexVertex2, edgesLength[0]);
    }
    if (length > 1) {
      tryToConnect(indexVertex2, indexVertex3, edgesLength[1]);
    }
    if (length > 2) {
      tryToConnect(indexVertex3, indexVertex1, edgesLength[2]);
    }
  }

  private void tryToConnect(int indexVertex1, int indexVertex2, int edgeLength) {
    JGraphEdge e = getGraph().addEdge(getGraphItems().get(indexVertex1), getGraphItems().get(indexVertex2));
    e.init(indexVertex1, indexVertex2, edgeLength);
    getGraph().setEdgeWeight(e, edgeLength);
  }
}