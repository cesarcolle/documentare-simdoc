package com.orange.documentare.core.comp.clustering.graph.graphbuilder;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.clustering.graph.check.CheckGraph;
import com.orange.documentare.core.comp.clustering.graph.clusters.ClusterTreatments;
import com.orange.documentare.core.comp.clustering.graph.clusters.ClustersCenters;
import com.orange.documentare.core.comp.clustering.graph.jgrapht.*;
import com.orange.documentare.core.comp.clustering.graph.subgraphs.SubGraphTreatments;
import com.orange.documentare.core.comp.clustering.graph.voronoi.Voronoi;
import com.orange.documentare.core.comp.measure.ProgressListener;
import com.orange.documentare.core.comp.measure.TreatmentStep;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.core.model.ref.clustering.ClusteringCoreParameters;
import com.orange.documentare.core.model.ref.clustering.ClusteringItem;
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters;
import com.orange.documentare.core.model.ref.clustering.graph.ClusteringGraph;
import com.orange.documentare.core.model.ref.clustering.graph.GraphCluster;
import com.orange.documentare.core.model.ref.clustering.graph.GraphItem;
import com.orange.documentare.core.system.measure.Progress;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.jgrapht.Graph;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.orange.documentare.core.comp.clustering.graph.clusters.BindClusterToSubgraph.bindClusterToSubgraph;
import static com.orange.documentare.core.comp.clustering.graph.clusters.BindClusterToSubgraph.updateClusterEdges;

@Slf4j
public class ClusteringGraphBuilder {

  @Setter
  private ProgressListener progressListener;

  private long t0;
  private int percent;

  public Clustering doClustering(ClusteringItem[] clusteringItems, ClusteringParameters parameters) {
    log.info(parameters.toString());
    t0 = System.currentTimeMillis();

    if (clusteringItems.length < 3) {
      return ClusteringForLT3Items.build(clusteringItems, parameters);
    }

    ClusteringGraph clusteringGraph = parameters.allInSameCluster ?
      buildAllInSameCluster(clusteringItems) :
      build(clusteringItems, parameters);

    percent = 100;
    onProgress(TreatmentStep.DONE);

    Clustering clustering = clusteringGraph.toCleanClusteringOutput(parameters);
    check(clustering, clusteringGraph);

    return clustering;
  }

  private ClusteringGraph buildAllInSameCluster(ClusteringItem[] noDuplicates) {
    // make sure there is no annoying parameter: rebuild a clean object
    ClusteringParameters params = ClusteringParameters.builder().allInSameCluster().build();
    List<GraphItem> graphItems = buildGraphItems(noDuplicates, params);
    ClusteringGraph clusteringGraph = new ClusteringGraph(graphItems);
    SubgraphsBuilder subgraphsBuilder = new SubgraphsBuilder(clusteringGraph);

    subgraphsBuilder.computeSubGraphs(new AllInSameClusterGraphBuilder());
    rebuildSubGraphsAndClusters(clusteringGraph, subgraphsBuilder);

    List<Integer> centers = ClustersCenters.findCenters(graphItems, clusteringGraph.getClusters().values());
    centers.forEach(index -> graphItems.get(index).setClusterCenter(true));
    AllInSameCluster.mergeMultiClusters(clusteringGraph);

    GraphCluster cluster = clusteringGraph.getClusters().values().iterator().next();
    cluster.initMultisetWith(centers);

    return clusteringGraph;
  }

  private ClusteringGraph build(ClusteringItem[] noDuplicates, ClusteringParameters parameters) {
    ClusteringGraph clusteringGraph = doBuild(noDuplicates, parameters);
    if (parameters.enroll) {
      log.info("===> Singletons recall");
      SingletonsRecall singletonsRecall = new SingletonsRecall(this, Arrays.asList(noDuplicates), parameters.enrollParameters);
      singletonsRecall.doRecallAndUpdateGraph(clusteringGraph);
    }
    return clusteringGraph;
  }

  ClusteringGraph doBuild(ClusteringItem[] clusteringItems, ClusteringCoreParameters parameters) {
    List<GraphItem> graphItems = buildGraphItems(clusteringItems, parameters);
    ClusteringGraph clusteringGraph = new ClusteringGraph(graphItems);
    SubgraphsBuilder subgraphsBuilder = new SubgraphsBuilder(clusteringGraph);

    triangulationTreatments(graphItems, parameters);
    subgraphsBuilder.computeSubGraphs(new TriangulationGraphBuilder());

    subGraphsTreatments(clusteringGraph, subgraphsBuilder, parameters);

    clustersPostTreatments(clusteringGraph, subgraphsBuilder, parameters);

    return clusteringGraph;
  }

  /**
   * if sloop is false, call treatment once, otherwise it loops
   */
  private void subGraphsTreatments(ClusteringGraph clusteringGraph, SubgraphsBuilder subgraphsBuilder, ClusteringCoreParameters parameters) {
    percent = 10;
    onProgress(TreatmentStep.SUBGRAPHS_POST_PROCESSING);

    int sloopLoops = 0;
    float variableScut = parameters.scutSdFactor;
    int subgraphNb;
    int clusterNb;
    do {
      doSubGraphTreatments(clusteringGraph, subgraphsBuilder, variableScut, parameters);

      subgraphNb = clusteringGraph.getSubGraphs().size();
      clusterNb = clusteringGraph.getClusters().values().size();

      variableScut -= 0.10;
      sloopLoops++;
    } while (parameters.sloop && subgraphNb != clusterNb && variableScut > 0);

    log.info("Subgraphs treatments, subgraphs = {}, clusters = {}, sloop({}), slooploops({})", subgraphNb, clusterNb, parameters.sloop, sloopLoops);
  }

  private void doSubGraphTreatments(ClusteringGraph clusteringGraph, SubgraphsBuilder subgraphsBuilder, float scutSdFactor, ClusteringCoreParameters parameters) {
    SubGraphTreatments subGraphTreatments = new SubGraphTreatments(clusteringGraph, parameters);
    subGraphTreatments.doTreatments(scutSdFactor);
    rebuildSubGraphsAndClusters(clusteringGraph, subgraphsBuilder);
  }


  private void triangulationTreatments(List<GraphItem> graphItems, ClusteringCoreParameters parameters) {
    onProgress(TreatmentStep.TRIANGULATION);

    TriangulationTreatments triangulationTreatments = new TriangulationTreatments(graphItems, parameters);
    triangulationTreatments.doTreatments();
  }

  private static List<GraphItem> buildGraphItems(ClusteringItem[] clusteringItems, ClusteringCoreParameters parameters) {
    int kNearestNeighboursThreshold = parameters.knn() ? parameters.kNearestNeighboursThreshold : clusteringItems.length;
    GraphItemsBuilder graphItemsBuilder = new GraphItemsBuilder(clusteringItems, kNearestNeighboursThreshold);
    return graphItemsBuilder.initGraphItems();
  }

  private void clustersPostTreatments(ClusteringGraph clusteringGraph, SubgraphsBuilder subgraphsBuilder, ClusteringCoreParameters parameters) {
    percent = 50;
    onProgress(TreatmentStep.CLUSTERS_POST_PROCESSING);
    ClusterTreatments clusterTreatments = new ClusterTreatments(clusteringGraph, parameters);
    if (parameters.ccut()) {
      clusterTreatments.cutLongestVertices();
      rebuildSubGraphsAndClusters(clusteringGraph, subgraphsBuilder);
    }

    List<GraphItem> items = clusteringGraph.getItems();
    List<Integer> centers = ClustersCenters.findCenters(items, clusteringGraph.getClusters().values());
    centers.forEach(index -> items.get(index).setClusterCenter(true));

    log.info("Clusters treatments, subgraphs = {}, clusters = {}", clusteringGraph.getSubGraphs().size(), clusteringGraph.getClusters().values().size());
  }

  private void check(Clustering clustering, ClusteringGraph clusteringGraph) {
    percent = 90;
    onProgress(TreatmentStep.CHECK_GRAPH);
    CheckGraph checkGraph = new CheckGraph(clustering, clusteringGraph);
    checkGraph.check();
    log.info("Graph check: OK");
  }

  private void rebuildSubGraphsAndClusters(ClusteringGraph clusteringGraph, SubgraphsBuilder subgraphsBuilder) {
    Graph<GraphItem, JGraphEdge> graph = subgraphsBuilder.computeSubGraphs(new InternalJGraphTBuilder());
    buildVoronoiClusters(clusteringGraph, graph);
    bindClusterToSubgraph(clusteringGraph);
    updateClusterEdges(clusteringGraph, graph.edgeSet().stream().map(JGraphEdge::getEdge).collect(Collectors.toList()));
  }

  /**
   * Use voronoi algo to detect graph regions, which may generate new subgraphs
   */
  private void buildVoronoiClusters(ClusteringGraph clusteringGraph, Graph<GraphItem, JGraphEdge> graph) {
    onProgress(TreatmentStep.VORONOI);
    clusteringGraph.getClusters().clear();
    Voronoi voronoi = new Voronoi(clusteringGraph, graph);
    voronoi.mapClusterId();
  }

  private void onProgress(TreatmentStep step) {
    if (progressListener != null) {
      progressListener.onProgressUpdate(step, new Progress(percent, (int) (System.currentTimeMillis() - t0) / 1000));
    }
  }
}
