package com.orange.documentare.core.comp.clustering.graph.graphbuilder;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the
 */

import com.orange.documentare.core.model.ref.clustering.graph.ClusteringGraph;
import com.orange.documentare.core.model.ref.clustering.graph.GraphCluster;
import com.orange.documentare.core.model.ref.clustering.graph.GraphEdge;
import com.orange.documentare.core.model.ref.clustering.graph.SubGraph;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

class AllInSameCluster {

  static void mergeMultiClusters(ClusteringGraph clusteringGraph) {
    Collection<GraphCluster> clusters = clusteringGraph.getClusters().values();
    GraphCluster biggerCluster = biggerCluster(clusters);
    int biggerClusterId = biggerCluster.getGroupId();

    updateItemsWith1ClusterCenter1Subgraph1Cluster(clusteringGraph, biggerClusterId);
    SubGraph subgraph = mergeSubgraphs(clusteringGraph);
    GraphCluster cluster = rebuildSingleCluster(clusteringGraph, subgraph.getEdges());

    clusteringGraph.getSubGraphs().clear();
    clusteringGraph.getSubGraphs().put(0, subgraph);
    clusteringGraph.getClusters().clear();
    clusteringGraph.getClusters().put(0, cluster);
  }

  private static GraphCluster rebuildSingleCluster(ClusteringGraph clusteringGraph, List<GraphEdge> allEdges) {
    GraphCluster cluster = new GraphCluster();
    cluster.setGroupId(0);
    cluster.setSubgraphId(0);
    cluster.getEdges().addAll(allEdges);
    clusteringGraph.getClusters().values().forEach(cl -> {
      cluster.getItemIndices().addAll(cl.getItemIndices());
    });
    return cluster;
  }

  private static SubGraph mergeSubgraphs(ClusteringGraph clusteringGraph) {
    SubGraph subgraph = new SubGraph();
    subgraph.setGroupId(0);
    subgraph.getClusterIndices().add(0);
    clusteringGraph.getSubGraphs().values().forEach(s -> {
      subgraph.getItemIndices().addAll(s.getItemIndices());
      subgraph.getEdges().addAll(s.getEdges());
    });
    return subgraph;
  }

  private static void updateItemsWith1ClusterCenter1Subgraph1Cluster(ClusteringGraph clusteringGraph, int biggerClusterId) {
    clusteringGraph.getItems().forEach(item -> {
      if (item.isClusterCenter() && item.getClusterId() != biggerClusterId) {
        item.setClusterCenter(false);
      }
      item.setSubgraphId(0);
      item.setClusterId(0);
    });
  }

  private static GraphCluster biggerCluster(Collection<GraphCluster> clusters) {
    return clusters.stream()
      .max(Comparator.comparingInt(cluster -> cluster.getItemIndices().size())).get();
  }
}
