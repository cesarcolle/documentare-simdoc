package com.orange.documentare.core.comp.clustering.graph.graphbuilder;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters;
import com.orange.documentare.core.model.ref.clustering.EnrollParameters;
import com.orange.documentare.core.comp.clustering.graph.ImportModel;
import com.orange.documentare.core.model.io.Gzip;
import com.orange.documentare.core.model.json.JsonGenericHandler;
import com.orange.documentare.core.model.ref.clustering.graph.ClusteringGraph;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class SingletonsRecallIntegrationTest {

  private static final String CLUSTERING_INPUT = "/bestioles_nearests_for_clustering.json.gz";
  private static final String GRAPH_OUTPUT_REF = "/bestioles_graph_singleton_recall_ref.json.gz";
  private static final String GRAPH_OUTPUT = "bestioles_graph_singleton_recall.json.gz";

  @After
  public void cleanup() {
    FileUtils.deleteQuietly(new File(GRAPH_OUTPUT));
  }

  @Test
  public void graph_is_built_with_singleton_recall() throws IOException {
    // given
    JsonGenericHandler jsonGenericHandler = new JsonGenericHandler(true);
    ImportModel importModel = (ImportModel) jsonGenericHandler.getObjectFromJsonGzipFile(ImportModel.class, new File(getClass().getResource(CLUSTERING_INPUT).getFile()));
    importModel.loadItemsBytes();
    ClusteringParameters parameters = ClusteringParameters.builder().acut().qcut()
      .enroll()
      .enrollParameters(EnrollParameters.builder().acut().qcut().disableSloop().build())
      .build();
    ClusteringGraphBuilder clusteringGraphBuilder = new ClusteringGraphBuilder();
    // do
    Clustering clustering = clusteringGraphBuilder.doClustering(importModel.getItems(), parameters);
    File output = new File(GRAPH_OUTPUT);
    jsonGenericHandler.writeObjectToJsonGzipFile(clustering.graph, output);
    // then
    File expected = new File(getClass().getResource(GRAPH_OUTPUT_REF).getFile());
    String expectedJsonString = Gzip.getStringFromGzipFile(expected);
    String outputJsonString = Gzip.getStringFromGzipFile(output);
    assertEquals(expectedJsonString, outputJsonString);
  }
}
