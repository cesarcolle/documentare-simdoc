package com.orange.documentare.core.comp.clustering.graph.graphbuilder;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.EnrollParameters;
import com.orange.documentare.core.comp.clustering.graph.Item;
import com.orange.documentare.core.model.ref.clustering.ClusteringItem;
import com.orange.documentare.core.model.ref.clustering.graph.*;
import org.fest.assertions.Assertions;
import org.junit.Test;

import java.util.*;
import java.util.stream.IntStream;

public class SingletonsRecallUpdateTest {

  private EnrollParameters parameters = EnrollParameters.builder().build();
  private ClusteringGraphBuilder clusteringGraphBuilder = new ClusteringGraphBuilder();
  private List<ClusteringItem> clusteringItems = buildClusteringItems();
  private SingletonsRecall singletonsRecall = new SingletonsRecall(clusteringGraphBuilder, clusteringItems, parameters);

  @Test
  public void initial_graph_singletons_items_are_updated_thanks_to_singletons_graph() {
    // given
    ClusteringGraph initialGraph = buildInitialGraph();
    ClusteringGraph singletonsGraph = buildSingletonsGraph();
    SingletonsRecallIndexMaps indexMaps = new SingletonsRecallIndexMaps(clusteringItems, clusteringItems);
    SingletonsRecall.GraphInfo initialGraphInfo = new SingletonsRecall.GraphInfo(initialGraph, indexMaps);

    // when
    singletonsRecall.updateInitialGraphSingletons(initialGraph, singletonsGraph, initialGraphInfo);

    // then
    GraphItem graphItem0 = initialGraph.getItems().get(0);
    GraphItem graphItem1 = initialGraph.getItems().get(1);
    GraphItem graphItem2 = initialGraph.getItems().get(2);
    Assertions.assertThat(graphItem0.getSubgraphId()).isEqualTo(33);
    Assertions.assertThat(graphItem0.getClusterId()).isEqualTo(321);
    Assertions.assertThat(graphItem0.isClusterCenter()).isTrue();
    Assertions.assertThat(graphItem0.enrolled()).isTrue();
    Assertions.assertThat(graphItem1.getSubgraphId()).isEqualTo(34);
    Assertions.assertThat(graphItem1.getClusterId()).isEqualTo(331);
    Assertions.assertThat(graphItem1.isClusterCenter()).isFalse();
    Assertions.assertThat(graphItem1.enrolled()).isTrue();
    Assertions.assertThat(graphItem2.getSubgraphId()).isEqualTo(34);
    Assertions.assertThat(graphItem2.getClusterId()).isEqualTo(331);
    Assertions.assertThat(graphItem1.isClusterCenter()).isFalse();
    Assertions.assertThat(graphItem2.enrolled()).isTrue();
  }

  @Test
  public void initial_graph_subgraphs_and_clusters_are_updated_thanks_to_singletons_graph() {
    // given
    ClusteringGraph initialGraph = buildInitialGraph();
    ClusteringGraph singletonsGraph = buildSingletonsGraph();
    SingletonsRecallIndexMaps indexMaps = new SingletonsRecallIndexMaps(clusteringItems, clusteringItems);
    SingletonsRecall.GraphInfo initialGraphInfo = new SingletonsRecall.GraphInfo(initialGraph, indexMaps);

    // when
    singletonsRecall.removeInitialGraphSingletonsFromSubgraphsAndClusters(initialGraph, initialGraphInfo);
    singletonsRecall.updateInitialGraphSubgraphAndClusters(initialGraph, singletonsGraph, initialGraphInfo);

    // then
    Map<Integer, SubGraph> subgraphs = initialGraph.getSubGraphs();
    Map<Integer, GraphCluster> clusters = initialGraph.getClusters();
    Assertions.assertThat(subgraphs).hasSize(2);
    Assertions.assertThat(subgraphs.containsKey(33)).isTrue();
    Assertions.assertThat(subgraphs.containsKey(34)).isTrue();
    Assertions.assertThat(subgraphs.get(33).getGroupId()).isEqualTo(33);
    Assertions.assertThat(subgraphs.get(34).getGroupId()).isEqualTo(34);
    Assertions.assertThat(subgraphs.get(33).getClusterIndices()).containsExactly(321);
    Assertions.assertThat(subgraphs.get(34).getClusterIndices()).containsExactly(331);
    Assertions.assertThat(subgraphs.get(33).getItemIndices()).containsExactly(0);
    Assertions.assertThat(subgraphs.get(34).getItemIndices()).containsExactly(1, 2);
    Assertions.assertThat(clusters).hasSize(2);
    Assertions.assertThat(clusters.containsKey(321)).isTrue();
    Assertions.assertThat(clusters.containsKey(331)).isTrue();
    Assertions.assertThat(clusters.get(321).getGroupId()).isEqualTo(321);
    Assertions.assertThat(clusters.get(331).getGroupId()).isEqualTo(331);
    Assertions.assertThat(clusters.get(321).getItemIndices()).containsExactly(0);
    Assertions.assertThat(clusters.get(331).getItemIndices()).containsExactly(1, 2);
  }

  @Test
  public void update_edges_should_update_and_reject_edges_not_belonging_to_subgraph() {
    // Given
    SingletonsRecall singletonsRecall = new SingletonsRecall(null, null, null);
    List<Integer> subgraphItemIndices = Arrays.asList(10, 20);
    GraphCluster graphGroup = new GraphCluster();

    List<GraphEdge> edges = graphGroup.getEdges();
    edges.add(new GraphEdge(1, 2, 1000));
    edges.add(new GraphEdge(1, 5, 1000));
    edges.add(new GraphEdge(5, 1, 1000));
    edges.add(new GraphEdge(5, 5, 1000));

    Map<Integer, Integer> newToOld = new HashMap<>();
    newToOld.put(1, 10);
    newToOld.put(2, 20);

    // When
    singletonsRecall.updateEdges(subgraphItemIndices, graphGroup, newToOld);

    // Then
    Assertions.assertThat(edges).containsExactly(new GraphEdge(10, 20, 1000));
  }

  private List<ClusteringItem> buildClusteringItems() {
    List<ClusteringItem> items = new ArrayList<>();
    IntStream.range(0, 3).forEach(i -> items.add(new Item(String.valueOf(i))));
    return items;
  }

  private ClusteringGraph buildInitialGraph() {
    List<GraphItem> items = new ArrayList<>();
    ClusteringGraph clusteringGraph = new ClusteringGraph(items);

    items.add(buildGraphItem(0, 100,10));
    items.add(buildGraphItem(1, 110,11));
    items.add(buildGraphItem(2, 120,12));

    clusteringGraph.getSubGraphs().put(10, buildSubGraph(10, 100, new int[]{0}));
    clusteringGraph.getSubGraphs().put(11, buildSubGraph(11, 110, new int[]{1}));
    clusteringGraph.getSubGraphs().put(12, buildSubGraph(12, 120, new int[]{2}));
    clusteringGraph.getClusters().put(100, buildCluster(10, 100, new int[]{0}));
    clusteringGraph.getClusters().put(110, buildCluster(11, 110, new int[]{1}));
    clusteringGraph.getClusters().put(120, buildCluster(12, 120, new int[]{2}));

    return clusteringGraph;
  }

  private ClusteringGraph buildSingletonsGraph() {
    List<GraphItem> items = new ArrayList<>();
    ClusteringGraph singletonsGraph = new ClusteringGraph(items);

    items.add(buildGraphItem(0, 200,20));
    items.add(buildGraphItem(1, 210,21));
    items.add(buildGraphItem(2, 210,21));

    items.get(0).setClusterCenter(true);

    singletonsGraph.getSubGraphs().put(20, buildSubGraph(20, 200, new int[]{0}));
    singletonsGraph.getSubGraphs().put(21, buildSubGraph(21, 210, new int[]{1, 2}));
    singletonsGraph.getClusters().put(200, buildCluster(20, 200, new int[]{0}));
    singletonsGraph.getClusters().put(210, buildCluster(21, 210, new int[]{1, 2}));

    return singletonsGraph;
  }

  private GraphItem buildGraphItem(int index, int clusterId, int subgraphId) {
    GraphItem graphItem = new GraphItem();
    graphItem.setVertex1Index(index);
    graphItem.setClusterId(clusterId);
    graphItem.setSubgraphId(subgraphId);
    return graphItem;
  }

  private SubGraph buildSubGraph(int subgraphId, int clusterId, int[] itemIndices) {
    SubGraph subGraph = new SubGraph();
    subGraph.setGroupId(subgraphId);
    subGraph.getClusterIndices().add(clusterId);
    Arrays.stream(itemIndices).forEach(index -> subGraph.getItemIndices().add(index));
    return subGraph;
  }

  private GraphCluster buildCluster(int subgraphId, int clusterId, int[] itemIndices) {
    GraphCluster cluster = new GraphCluster();
    cluster.setSubgraphId(subgraphId);
    cluster.setGroupId(clusterId);
    Arrays.stream(itemIndices).forEach(index -> cluster.getItemIndices().add(index));
    return cluster;
  }
}
