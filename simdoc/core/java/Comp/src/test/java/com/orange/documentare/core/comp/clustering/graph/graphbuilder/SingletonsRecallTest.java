package com.orange.documentare.core.comp.clustering.graph.graphbuilder;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters;
import com.orange.documentare.core.model.ref.clustering.EnrollParameters;
import com.orange.documentare.core.comp.clustering.graph.Item;
import com.orange.documentare.core.model.ref.clustering.ClusteringItem;
import com.orange.documentare.core.model.ref.clustering.graph.ClusteringGraph;
import com.orange.documentare.core.model.ref.comp.NearestItem;
import org.fest.assertions.Assertions;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/** Based on a given graph, check we can retrieve singletons from the built graph */
public class SingletonsRecallTest implements Item.ItemInit {

  @Test
  public void compute_graph_with_singleton_recall() {
    // given
    ClusteringItem[] clusteringItems = Item.buildClusteringItems(this, 7);
    ClusteringParameters parameters = ClusteringParameters.builder().acut(0.1f).qcut()
      .enroll().enrollParameters(EnrollParameters.builder().acut(0.1f).qcut().disableSloop().build())
      .build();
    ClusteringGraphBuilder clusteringGraphBuilder = new ClusteringGraphBuilder();

    // when
    Clustering clustering = clusteringGraphBuilder.doClustering(clusteringItems, parameters);
    ClusteringGraph clusteringGraph = clustering.graph;

    // then
    Assertions.assertThat(clusteringGraph.getSubGraphs()).hasSize(3);
    Assertions.assertThat(clusteringGraph.getClusters()).hasSize(3);
    Assertions.assertThat(clusteringGraph.getSubGraphs().get(0).getItemIndices()).containsExactly(0, 1, 2);
    Assertions.assertThat(clusteringGraph.getSubGraphs().get(5).getItemIndices()).containsExactly(3, 4, 5);
    Assertions.assertThat(clusteringGraph.getSubGraphs().get(6).getItemIndices()).containsExactly(6);
    Assertions.assertThat(clusteringGraph.getClusters().get(1).getItemIndices()).containsExactly(0, 1, 2);
    Assertions.assertThat(clusteringGraph.getClusters().get(7).getItemIndices()).containsExactly(3, 4, 5);
    Assertions.assertThat(clusteringGraph.getClusters().get(8).getItemIndices()).containsExactly(6);
  }

  @Test
  public void compute_graph_and_retrieve_singletons() {
    // given
    ClusteringItem[] clusteringItems = Item.buildClusteringItems(this, 7);
    EnrollParameters parameters = EnrollParameters.builder().acut(0.1f).qcut().build();
    ClusteringGraphBuilder clusteringGraphBuilder = new ClusteringGraphBuilder();
    SingletonsRecall singletonsRecall = new SingletonsRecall(clusteringGraphBuilder, Arrays.asList(clusteringItems), parameters);

    ClusteringGraph clusteringGraph = clusteringGraphBuilder.doBuild(clusteringItems, parameters);

    // do
    ClusteringItem[] singletons = singletonsRecall.retrieveSingletonsItemsFrom(Arrays.asList(clusteringItems), clusteringGraph);

    // then
    Assertions.assertThat(singletons).hasSize(4);
    Assertions.assertThat(singletons).contains(clusteringItems[3], clusteringItems[4], clusteringItems[5], clusteringItems[6]);
  }

  @Test
  public void build_new_singletons_objects_to_do_second_graph_build_on_singletons_only() {
    // given
    ClusteringItem[] clusteringItems = Item.buildClusteringItems(this, 7);
    ClusteringParameters parameters = ClusteringParameters.builder().acut(0.1f).qcut().build();
    ClusteringGraphBuilder clusteringGraphBuilder = new ClusteringGraphBuilder();
    SingletonsRecall singletonsRecall = new SingletonsRecall(clusteringGraphBuilder, Arrays.asList(clusteringItems), parameters.enrollParameters);

    Clustering clustering = clusteringGraphBuilder.doClustering(clusteringItems, parameters);
    ClusteringItem[] singletons = singletonsRecall.retrieveSingletonsItemsFrom(Arrays.asList(clusteringItems), clustering.graph);


    // To make sure we can rely on singleton index in our assertions
    Arrays.sort(singletons, Comparator.comparing(ClusteringItem::getHumanReadableId));

    List<ClusteringItem> clusteringItemsList = Arrays.asList(clusteringItems);
    SingletonsRecallIndexMaps indexMaps = new SingletonsRecallIndexMaps(clusteringItemsList, Arrays.asList(singletons));

    // do
    SingletonForReGraph[] singletonsForRegraph = singletonsRecall.buildSingletonsForRegraph(indexMaps.oldToNew, clusteringItemsList, singletons);

    // then
    Assertions.assertThat(singletons).hasSize(4);
    SingletonForReGraph singleton0 = singletonsForRegraph[0];
    Assertions.assertThat(singleton0.getNearestItems()[0].getIndex()).isEqualTo(0);
    Assertions.assertThat(singleton0.getNearestItems()[1].getIndex()).isEqualTo(1);
    Assertions.assertThat(singleton0.getNearestItems()[2].getIndex()).isEqualTo(2);
    Assertions.assertThat(singleton0.getNearestItems()[3].getIndex()).isEqualTo(3);

    SingletonForReGraph singleton2 = singletonsForRegraph[2];
    Assertions.assertThat(singleton2.getNearestItems()[0].getIndex()).isEqualTo(2);
    Assertions.assertThat(singleton2.getNearestItems()[1].getIndex()).isEqualTo(1);
    Assertions.assertThat(singleton2.getNearestItems()[2].getIndex()).isEqualTo(0);
    Assertions.assertThat(singleton2.getNearestItems()[3].getIndex()).isEqualTo(3);
  }

  @Override
  public void init(Item[] items) {
    items[0].setNearestItems(new NearestItem[]{ new NearestItem(0, 0), new NearestItem(1, 10), new NearestItem(2, 10), new NearestItem(3, 200), new NearestItem(4, 200), new NearestItem(5, 200), new NearestItem(6, 1000)});
    items[1].setNearestItems(new NearestItem[]{ new NearestItem(1, 0), new NearestItem(0, 10), new NearestItem(2, 10), new NearestItem(3, 200), new NearestItem(4, 200), new NearestItem(5, 200), new NearestItem(6, 1000)});
    items[2].setNearestItems(new NearestItem[]{ new NearestItem(2, 0), new NearestItem(0, 10), new NearestItem(1, 10), new NearestItem(3, 200), new NearestItem(4, 200), new NearestItem(5, 200), new NearestItem(6, 1000)});

    items[3].setNearestItems(new NearestItem[]{ new NearestItem(3, 0), new NearestItem(4, 100), new NearestItem(5, 102), new NearestItem(0, 200), new NearestItem(1, 200), new NearestItem(2, 200), new NearestItem(6, 1000)});
    items[4].setNearestItems(new NearestItem[]{ new NearestItem(4, 0), new NearestItem(3, 100), new NearestItem(5, 101), new NearestItem(0, 200), new NearestItem(1, 200), new NearestItem(2, 200), new NearestItem(6, 1000)});
    items[5].setNearestItems(new NearestItem[]{ new NearestItem(5, 0), new NearestItem(4, 101), new NearestItem(3, 102), new NearestItem(0, 200), new NearestItem(1, 200), new NearestItem(2, 200), new NearestItem(6, 1000)});

    items[6].setNearestItems(new NearestItem[]{ new NearestItem(6, 0), new NearestItem(0, 1000), new NearestItem(1, 1000), new NearestItem(2, 1000), new NearestItem(3, 1000), new NearestItem(4, 1000), new NearestItem(5, 1000)});
  }
}
