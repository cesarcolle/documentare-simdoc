package com.orange.documentare.core.comp.clustering.graph;
/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.json.JsonGenericHandler;
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters;
import com.orange.documentare.core.model.ref.clustering.EnrollParameters;
import org.fest.assertions.Assertions;
import org.fest.util.Files;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.fest.assertions.Assertions.*;

public class ClusteringParametersTest {
  private static File JSON = new File("parameters.json");

  @Before
  @After
  public void cleanup() {
    Files.delete(JSON);
  }

  @Test
  public void build_parameters_with_default_values() {
    // Given

    // When
    ClusteringParameters parameters = ClusteringParameters.builder().build();

    // Then
    assertThat(parameters.qcut()).isFalse();
    assertThat(parameters.acut()).isFalse();
    assertThat(parameters.scut()).isFalse();
    assertThat(parameters.ccut()).isFalse();
    assertThat(parameters.wcut).isFalse();
    assertThat(parameters.knn()).isFalse();
    assertThat(parameters.sloop).isFalse();
    assertThat(parameters.allInSameCluster).isFalse();
    assertThat(parameters.enroll).isFalse();
    assertThat(parameters.enrollParameters.sloop).isTrue();
    assertThat(parameters.enrollParameters.scutSdFactor).isEqualTo(4);
  }

  @Test
  public void build_parameters() {
    // Given

    // When
    ClusteringParameters parameters = ClusteringParameters.builder()
            .acut()
            .qcut()
            .scut()
            .ccut()
            .wcut()
            .sloop()
            .allInSameCluster()
            .enroll()
            .enrollParameters(
              EnrollParameters.builder()
                .disableSloop()
                .acut()
                .qcut()
                .build())
            .build();

    // Then
    assertThat(parameters.qcut()).isTrue();
    assertThat(parameters.acut()).isTrue();
    assertThat(parameters.scut()).isTrue();
    assertThat(parameters.scutSdFactor).isEqualTo(ClusteringParameters.SCUT_DEFAULT_SD_FACTOR);
    assertThat(parameters.ccut()).isTrue();
    assertThat(parameters.wcut).isTrue();
    assertThat(parameters.knn()).isFalse();
    assertThat(parameters.sloop).isTrue();
    assertThat(parameters.allInSameCluster).isTrue();
    assertThat(parameters.enroll).isTrue();
    assertThat(parameters.enrollParameters.sloop).isFalse();
    assertThat(parameters.enrollParameters.acutSdFactor).isEqualTo(2);
    assertThat(parameters.enrollParameters.qcutSdFactor).isEqualTo(2);
  }


  @Test
  public void build_parameters_with_non_defaults() {
    // Given

    // When
    ClusteringParameters parameters = ClusteringParameters.builder()
            .acut(0.1f)
            .qcut(0.2f)
            .scut(0.3f)
            .ccut(23)
            .knn(12)
            .build();

    // Then
    assertThat(parameters.acutSdFactor).isEqualTo(0.1f);
    assertThat(parameters.qcutSdFactor).isEqualTo(0.2f);
    assertThat(parameters.scutSdFactor).isEqualTo(0.3f);
    assertThat(parameters.ccutPercentile).isEqualTo(23);
    assertThat(parameters.kNearestNeighboursThreshold).isEqualTo(12);
  }

  @Test
  public void build_parameters_with_sloop() {
    // Given

    // When
    ClusteringParameters parameters = ClusteringParameters.builder()
      .sloop()
      .build();

    // Then
    assertThat(parameters.scutSdFactor).isEqualTo(3);
    assertThat(parameters.sloop).isTrue();
  }

  @Test
  public void build_parameters_with_sloop_and_scut_std_factor_value() {
    // Given

    // When
    ClusteringParameters parameters = ClusteringParameters.builder()
      .sloop()
      .scut(1.1f)
      .build();

    // Then
    assertThat(parameters.scutSdFactor).isEqualTo(1.1f);
    assertThat(parameters.sloop).isTrue();
  }

  @Test
  public void can_write_and_read_json() throws IOException {
    // given
    ClusteringParameters parameters = ClusteringParameters.builder().build();

    // when
    JsonGenericHandler.instance().writeObjectToJsonFile(parameters, JSON);
    ClusteringParameters actualParams = (ClusteringParameters)
      JsonGenericHandler.instance().getObjectFromJsonFile(ClusteringParameters.class, JSON);

    // then
    assertThat(actualParams).isEqualTo(parameters);
  }
}
