package com.orange.documentare.core.comp.clustering.graph;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.comp.clustering.graph.graphbuilder.ClusteringGraphBuilder;
import com.orange.documentare.core.model.io.Gzip;
import com.orange.documentare.core.model.json.JsonGenericHandler;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters;
import com.orange.documentare.core.model.ref.clustering.graph.ClusteringGraph;
import org.apache.commons.io.FileUtils;
import org.fest.assertions.Assertions;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.fest.assertions.Assertions.assertThat;

public class ClusteringGraphBuilderAreaQTest {
  private static final String CLUSTERING_INPUT = "/bestioles_nearests_for_clustering.json.gz";
  private static final String GRAPH_OUTPUT_REF = "/bestioles_graph_ref.json.gz";
  private static final File GRAPH_OUTPUT = new File("bestioles_graph.json.gz");

  @After
  public void cleanup() {
    FileUtils.deleteQuietly(GRAPH_OUTPUT);
  }

  @Test
  public void shouldBuildGraphFromClusteringItemsInput() throws IOException {
    // given
    JsonGenericHandler jsonGenericHandler = new JsonGenericHandler(true);
    ImportModel importModel = (ImportModel) jsonGenericHandler.getObjectFromJsonGzipFile(ImportModel.class, new File(getClass().getResource(CLUSTERING_INPUT).getFile()));
    importModel.loadItemsBytes();
    ClusteringParameters parameters = ClusteringParameters.builder().acut().qcut().build();
    ClusteringGraphBuilder clusteringGraphBuilder = new ClusteringGraphBuilder();

    // do
    Clustering clustering = clusteringGraphBuilder.doClustering(importModel.getItems(), parameters);
    jsonGenericHandler.writeObjectToJsonGzipFile(clustering, GRAPH_OUTPUT);

    // then
    File expected = new File(getClass().getResource(GRAPH_OUTPUT_REF).getFile());
    String expectedJsonString = Gzip.getStringFromGzipFile(expected);
    String outputJsonString = Gzip.getStringFromGzipFile(GRAPH_OUTPUT);
    assertThat(outputJsonString).isEqualTo(expectedJsonString);

    // Check deserialization
    JsonGenericHandler.instance().getObjectFromJsonGzipFile(Clustering.class, GRAPH_OUTPUT);
  }
}
