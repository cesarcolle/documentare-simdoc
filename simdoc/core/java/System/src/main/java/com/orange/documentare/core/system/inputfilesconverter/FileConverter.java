package com.orange.documentare.core.system.inputfilesconverter;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.google.common.primitives.Bytes;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public interface FileConverter {
  void convert(FilesToConcat source, File destination);
}
