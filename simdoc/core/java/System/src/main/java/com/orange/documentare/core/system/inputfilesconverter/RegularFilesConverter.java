package com.orange.documentare.core.system.inputfilesconverter;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.google.common.primitives.Bytes;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class RegularFilesConverter implements FileConverter {

  @Override
  public void convert(FilesToConcat source, File destination) {
    byte[] bytes = {};
    for (File file : source) {
      try {
        bytes = Bytes.concat(bytes, FileUtils.readFileToByteArray(file));
      } catch (IOException e) {
        throw new FileConverterException(String.format("Failed to load file '%s'", file.getAbsolutePath()));
      }
    }

    try {
      FileUtils.writeByteArrayToFile(destination, bytes);
    } catch (IOException e) {
      throw new FileConverterException(String.format("Failed to write file '%s'", destination.getAbsolutePath()));
    }
  }
}
