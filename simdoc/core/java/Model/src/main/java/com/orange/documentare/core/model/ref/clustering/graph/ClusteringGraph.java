package com.orange.documentare.core.model.ref.clustering.graph;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.core.model.ref.clustering.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
@ToString
@RequiredArgsConstructor
@EqualsAndHashCode
public class ClusteringGraph {
  private final List<GraphItem> items;
  private final Map<Integer, SubGraph> subGraphs = new HashMap<>();
  private final Map<Integer, GraphCluster> clusters = new HashMap<>();

  public int clusterIdMax() {
    return items.stream()
      .mapToInt(graphItem -> graphItem.getClusterId())
      .max().getAsInt();
  }

  public int subgraphIdMax() {
    return items.stream()
      .mapToInt(graphItem -> graphItem.getSubgraphId())
      .max().getAsInt();
  }

  public Clustering toCleanClusteringOutput(ClusteringParameters parameters) {
    List<ClusteringElement> clusteringElements = Collections.unmodifiableList(
      items.stream()
        .map(it -> new ClusteringElement(it.getVertexName(), it.getClusterId(), it.isClusterCenter() ? true : null, it.getEnrolled(), null, null))
        .collect(Collectors.toList())
    );

    List<Subgraph> subgraphs = Collections.unmodifiableList(
      subGraphs.values().stream()
        .map(it -> new Subgraph(it.getItemIndices(),
          it.getEdges().stream()
            .map(g -> new Edge(g.getVertex1Index(), g.getVertex2Index(), g.getLength()))
            .collect(Collectors.toList()), null)
        )
        .collect(Collectors.toList())
    );

    List<Cluster> clusters = Collections.unmodifiableList(
      this.clusters.values().stream()
        .map(it -> new Cluster(it.getItemIndices(), it.getMultisetElementsIndices()))
        .collect(Collectors.toList())
    );

    return new Clustering(clusteringElements, subgraphs, clusters, parameters, this, null);
  }
}
