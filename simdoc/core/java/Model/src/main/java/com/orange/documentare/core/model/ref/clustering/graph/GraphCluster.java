package com.orange.documentare.core.model.ref.clustering.graph;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/** Cluster built by running Voronoi's algo on a subgraph */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Setter
@Getter
@ToString
@EqualsAndHashCode
public class GraphCluster extends GraphGroup {
  private int subgraphId;
  private List<Integer> multisetElementsIndices;

  public void initMultisetWith(List<Integer> centers) {
    multisetElementsIndices = centers;
  }
}
