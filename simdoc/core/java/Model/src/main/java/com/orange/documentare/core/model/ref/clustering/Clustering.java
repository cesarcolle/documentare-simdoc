package com.orange.documentare.core.model.ref.clustering;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.orange.documentare.core.model.ref.clustering.graph.ClusteringGraph;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
@RequiredArgsConstructor
@EqualsAndHashCode
public class Clustering {
  public final List<ClusteringElement> elements;
  public final List<Subgraph> subgraphs;
  public final List<Cluster> clusters;
  public final ClusteringParameters parameters;
  public final ClusteringGraph graph;
  public final String error;

  public static Clustering error(ClusteringParameters parameters, String message) {
    return new Clustering(null, null, null, parameters, null, message);
  }

  public boolean failed() {
    return error != null;
  }

  public Clustering dropGraph() {
    return new Clustering(elements, subgraphs, clusters, parameters, null, error);
  }

  public Clustering updateElementsId(List<ClusteringElement> updatedElements) {
    return new Clustering(updatedElements, subgraphs, clusters, parameters, graph, error);
  }
}
