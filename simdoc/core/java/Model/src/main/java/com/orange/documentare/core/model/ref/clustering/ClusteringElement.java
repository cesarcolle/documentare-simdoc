package com.orange.documentare.core.model.ref.clustering;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.Optional;

@JsonInclude(JsonInclude.Include.NON_NULL)
@RequiredArgsConstructor
@EqualsAndHashCode
public class ClusteringElement {
  public final String id;
  @Getter
  public final int clusterId;
  public final Boolean clusterCenter;
  public final Boolean enrolled;
  public final Integer duplicateOf;
  public final Integer distanceToCenter;

  public boolean clusterCenter() {
    return clusterCenter != null && clusterCenter;
  }
  public boolean enrolled() {
    return enrolled != null && enrolled;
  }
  public Optional<Integer> duplicateOf() {
    return Optional.ofNullable(duplicateOf);
  }

  public static ClusteringElement duplicate(String id, Integer duplicateOf) {
    return new ClusteringElement(id, -1, null, null, duplicateOf, null);
  }
}
