package com.orange.documentare.core.model.ref.clustering;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@EqualsAndHashCode
public final class ClusteringParameters extends ClusteringCoreParameters {
  public final boolean enroll;
  public final EnrollParameters enrollParameters;

  private ClusteringParameters(float qcutSdFactor, float acutSdFactor, float scutSdFactor, int ccutPercentile, boolean wcut, int kNearestNeighboursThreshold, boolean sloop, boolean allInSameCluster, boolean enroll, EnrollParameters enrollParameters) {
    super(qcutSdFactor, acutSdFactor, scutSdFactor, ccutPercentile, wcut, kNearestNeighboursThreshold, sloop, allInSameCluster);
    this.enroll = enroll;
    this.enrollParameters = enrollParameters;
  }

  @Override
  public String toString() {
    String str = super.toString();
    str += ", enroll=" + enroll;
    str += ", enrollParameters=" + enrollParameters;
    return str;
  }

  public static ClusteringParametersBuilder builder() {
    return new ClusteringParametersBuilder();
  }

  public static class ClusteringParametersBuilder {
    private static final int SLOOP_SCUT_SD_FACTOR_DEFAULT = 3;
    private float qcutSdFactor = -1;
    private float acutSdFactor = -1;
    private float scutSdFactor = -1;
    private int ccutPercentile = -1;
    private boolean wcut;
    private int kNearestNeighboursThreshold = -1;
    private boolean sloop;
    private boolean allInSameCluster;
    private boolean enroll;
    private EnrollParameters enrollParameters = EnrollParameters.builder().build();

    private ClusteringParametersBuilder() {
    }

    public ClusteringParameters build() {
      if (sloop && scutSdFactor < 0) {
        scutSdFactor = SLOOP_SCUT_SD_FACTOR_DEFAULT;
      }
      return new ClusteringParameters(qcutSdFactor, acutSdFactor, scutSdFactor, ccutPercentile, wcut, kNearestNeighboursThreshold, sloop, allInSameCluster, enroll, enrollParameters);
    }

    public ClusteringParametersBuilder qcut() {
      return qcut(Q_DEFAULT_SD_FACTOR);
    }
    public ClusteringParametersBuilder qcut(float qcutSdFactor) {
      this.qcutSdFactor = qcutSdFactor;
      return this;
    }

    public ClusteringParametersBuilder acut() {
      return acut(A_DEFAULT_SD_FACTOR);
    }
    public ClusteringParametersBuilder acut(float acutSdFactor) {
      this.acutSdFactor = acutSdFactor;
      return this;
    }

    public ClusteringParametersBuilder scut() {
      return scut(SCUT_DEFAULT_SD_FACTOR);
    }
    public ClusteringParametersBuilder scut(float scutSdFactor) {
      this.scutSdFactor = scutSdFactor;
      return this;
    }

    public ClusteringParametersBuilder ccut() {
      return ccut(CCUT_DEFAULT_PERCENTILE);
    }
    public ClusteringParametersBuilder ccut(int ccutPercentile) {
      this.ccutPercentile = ccutPercentile;
      return this;
    }

    public ClusteringParametersBuilder wcut() {
      wcut = true;
      return this;
    }

    public ClusteringParametersBuilder knn(int kNearestNeighboursThreshold) {
      this.kNearestNeighboursThreshold = kNearestNeighboursThreshold;
      return this;
    }

    public ClusteringParametersBuilder sloop() {
      sloop = true;
      return this;
    }

    public ClusteringParametersBuilder allInSameCluster() {
      allInSameCluster = true;
      return this;
    }

    public ClusteringParametersBuilder enroll() {
      enroll = true;
      return this;
    }

    public ClusteringParametersBuilder enrollParameters(EnrollParameters enrollParameters) {
      this.enrollParameters = enrollParameters;
      return this;
    }
  }
}
