package com.orange.documentare.core.model.ref.clustering;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@RequiredArgsConstructor
@EqualsAndHashCode
public class Cluster {
  public final List<Integer> elementsIndices;
  public final List<Integer> multisetElementsIndices;

  public Cluster(List<Integer> elementsIndices) {
    this(elementsIndices, null);
  }
}
