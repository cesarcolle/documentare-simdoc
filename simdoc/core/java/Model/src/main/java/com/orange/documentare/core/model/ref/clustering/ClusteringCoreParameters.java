package com.orange.documentare.core.model.ref.clustering;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

@EqualsAndHashCode
@RequiredArgsConstructor
public abstract class ClusteringCoreParameters {
  public static final float A_DEFAULT_SD_FACTOR = 2;
  public static final float Q_DEFAULT_SD_FACTOR = 2;
  public static final float SCUT_DEFAULT_SD_FACTOR = 2;
  public static final int CCUT_DEFAULT_PERCENTILE = 75;

  public final float qcutSdFactor;
  public final float acutSdFactor;
  public final float scutSdFactor;
  public final int ccutPercentile;
  public final boolean wcut;
  public final int kNearestNeighboursThreshold;
  public final boolean sloop;
  public final boolean allInSameCluster;

  public boolean acut() {
    return acutSdFactor > 0;
  }
  public boolean qcut() {
    return qcutSdFactor > 0;
  }
  public boolean scut() {
    return scutSdFactor > 0;
  }
  public boolean ccut() {
    return ccutPercentile > 0;
  }
  public boolean knn() {
    return kNearestNeighboursThreshold > 0;
  }

  @Override
  public String toString() {
    String str = String.format("acut=" + acut() + ", qcut=" + qcut() + ", scut=" + scut() + ", ccut=" + ccut() + ", wcut=" + wcut + ", knn=" + knn() + ", sloop=" + sloop + ", allInSameCluster=" + allInSameCluster);
    str += acut() ? ", acutSd=" + acutSdFactor : "";
    str += qcut() ? ", qcutSd=" + qcutSdFactor : "";
    str += scut() ? ", scutSd=" + scutSdFactor : "";
    str += ccut() ? ", ccutSd=" + ccutPercentile : "";
    str += knn() ? ", knn=" + kNearestNeighboursThreshold : "";
    return str;
  }

  // for deserialization...
  public ClusteringCoreParameters() {
    acutSdFactor = 0;
    qcutSdFactor = 0;
    scutSdFactor = 0;
    ccutPercentile = 0;
    wcut = false;
    kNearestNeighboursThreshold = 0;
    sloop = false;
    allInSameCluster = false;
  }
}
