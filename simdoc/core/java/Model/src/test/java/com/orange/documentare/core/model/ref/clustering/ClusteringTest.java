package com.orange.documentare.core.model.ref.clustering;

import com.orange.documentare.core.model.json.JsonGenericHandler;
import com.orange.documentare.core.model.ref.clustering.graph.ClusteringGraph;
import com.orange.documentare.core.model.ref.clustering.graph.GraphItem;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

public class ClusteringTest {

  private static final File OUTPUT_JSON = new File("clustering_ser_deser.test");

  @After
  public void cleanup() {
    FileUtils.deleteQuietly(OUTPUT_JSON);
  }

  @Test
  public void we_can_serialize_then_deserialize_a_clustering_object() throws IOException {
    // given
    ClusteringParameters parameters = ClusteringParameters.builder().build();
    String error = null;

    Clustering clustering = new Clustering(elements(), subgraphs(), clusters(), parameters, clusteringGraph(), error);

    // when
    JsonGenericHandler.instance().writeObjectToJsonFile(clustering, OUTPUT_JSON);
    JsonGenericHandler.instance().getObjectFromJsonFile(Clustering.class, OUTPUT_JSON);

    // then
    // no exception...
  }

  @Test
  public void a_clustering_object_is_equatable() throws IOException {
    // given
    ClusteringParameters parameters = ClusteringParameters.builder().build();
    String error = null;

    Clustering clustering = new Clustering(elements(), subgraphs(), clusters(), parameters, clusteringGraph(), error);

    // when
    JsonGenericHandler.instance().writeObjectToJsonFile(clustering, OUTPUT_JSON);
    Clustering clusteringFromDisk = (Clustering) JsonGenericHandler.instance().getObjectFromJsonFile(Clustering.class, OUTPUT_JSON);

    // then
    assertThat(clusteringFromDisk).isEqualTo(clustering);
  }

  @Test
  public void a_clustering_with_error_is_marked_failed() throws IOException {
    // given
    String errorMessage = "this is an error";

    // when
    Clustering clustering = Clustering.error(ClusteringParameters.builder().build(), errorMessage);

    // then
    assertThat(clustering.failed()). isTrue();
    assertThat(clustering.error).isEqualTo(errorMessage);
  }

  private List<ClusteringElement> elements() {
    List<ClusteringElement> elements = new ArrayList<>();
    elements.add(new ClusteringElement("1", 1, true, true, null, null));
    return elements;
  }

  private List<Subgraph> subgraphs() {
    List<Subgraph> subgraphs = new ArrayList<>();

    List<Integer> elementsIndices = new ArrayList<>();
    elementsIndices.add(10);

    List<Edge> edges = new ArrayList<>();
    edges.add(new Edge(100, 200, 10000));

    int distanceThreshold = 1234;

    subgraphs.add(new Subgraph(elementsIndices, edges, distanceThreshold));
    return subgraphs;
  }

  private List<Cluster> clusters() {
    List<Cluster> clusters = new ArrayList<>();

    List<Integer> elementsIndices = new ArrayList<>();
    elementsIndices.add(10);

    clusters.add(new Cluster(elementsIndices));
    return clusters;
  }

  private ClusteringGraph clusteringGraph() {
    List<GraphItem> items = new ArrayList<>();
    GraphItem graphItem = new GraphItem();
    graphItem.setVertexName("titi");
    items.add(graphItem);
    ClusteringGraph clusteringGraph = new ClusteringGraph(items);
    return clusteringGraph;
  }
}
