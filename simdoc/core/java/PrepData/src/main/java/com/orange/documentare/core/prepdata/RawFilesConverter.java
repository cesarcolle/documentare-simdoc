package com.orange.documentare.core.prepdata;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */


import com.google.common.primitives.Bytes;
import com.orange.documentare.core.image.opencv.OpenCvImage;
import com.orange.documentare.core.system.inputfilesconverter.FileConverter;
import com.orange.documentare.core.system.inputfilesconverter.FilesToConcat;
import com.orange.documentare.core.system.inputfilesconverter.RegularFilesConverter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.opencv.core.Mat;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

@RequiredArgsConstructor
public class RawFilesConverter implements FileConverter {

  private static final String[] IMAGES_EXTENSION = {
    ".png", ".jpg", ".jpeg", ".tif", ".tiff", ".bmp"
  };

  private final RegularFilesConverter regularFilesConverter = new RegularFilesConverter();

  private final int expectedPixelsCount;

  @Override
  public void convert(FilesToConcat source, File destination) {
    File firstFile = source.get(0);
    if (isImage(firstFile)) {
      convertToRaw(source, destination);
    } else {
      regularFilesConverter.convert(source, destination);
    }
  }

  public void convertToRaw(FilesToConcat source, File destination) {
    byte[] concatBytes = {};
    for (File file : source) {
      Mat mat = OpenCvImage.loadMat(file);
      if (expectedPixelsCount > 0) {
        mat = OpenCvImage.resize(mat, expectedPixelsCount);
      }
      byte[] bytes = OpenCvImage.matToRaw(mat);
      concatBytes = Bytes.concat(concatBytes, bytes);
    }

    try {
      FileUtils.writeByteArrayToFile(destination, concatBytes);
    } catch (IOException e) {
      throw new IllegalStateException(String.format("Failed to write raw file to '%s': %s", destination.getAbsolutePath(), e.getMessage()));
    }
  }

  public boolean isImage(File source) {
    String lowerCaseFilename = source.getName().toLowerCase();
    return Arrays.stream(IMAGES_EXTENSION)
      .filter(ext -> lowerCaseFilename.endsWith(ext))
      .count() > 0;
  }
}
