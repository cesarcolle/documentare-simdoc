# Simdoc servers docker usage

Docker images can be found in the project docker registry: https://gitlab.com/Orange-OpenSource/documentare/documentare-simdoc/container_registry


To pull and run the image: `docker run -p2407:2407 -v/home/jojo/data:/data -e JAVA_MEM=2g registry.gitlab.com/orange-opensource/documentare/documentare-simdoc:clustering_mediation-UPDATE_VERSION_PLEASE`

 - `-p2407:2407` to expose container port 2407 to localhost on port 2407
 - `-v /home/jojo/data:/data` to mount host directory (i.e. your client computer) `/home/jojo/data` on `/data` which is the mounted directory name in container (i.e. the docker container which is a virtual machine running on a distant server). All classical mounting rules are applicables here and you have to adapt this parameter depending your environment.
 - `-e JAVA_MEM=2g` to start the server with 2 giga bytes of memory
 - `UPDATE_VERSION_PLEASE`: you can find image tag here, https://gitlab.com/Orange-OpenSource/documentare/documentare-simdoc/container_registry

## Request example to test it with swagger UI (http://localhost:2407/):
```
{
  "acutSdFactor": 2,
  "qcutSdFactor": 2,
  "debug": true,
  "inputDirectory": "/data/bestioles",
  "outputDirectory": "/data/out"
}
```

## Build the graph
As we have set the debug option (`"debug": true`), we can find in the `outputDirectory` the required files to build the clustering graph.

To build the graph, you will first need to install all tools:
 - download the last simdoc debian package: https://gitlab.com/Orange-OpenSource/documentare/documentare-simdoc/pipelines
 - and install it: `sudo dpkg -i simdoc-x.y.z.deb` followed by `sudo apt-get -f install` to install missing dependencies

Then you will find all tools under the following directory: `/usr/share/java`

To build the graph:
 - generate the `graph.dot` file: `java -jar /usr/share/java/graph.jar -json /data/out/clustering-graph.json.gz -metadata /data/out/metadata.json`
 - generate the pdf file: `twopi graph.dot | gvmap -e | neato -Ecolor="#55555522" -n2 -Tpdf > graph.pdf`

# Docker survival guide

 - to check if docker works correctly on the machine: `docker run hello-world`
 - to ps: `docker ps`
 - then to kill: `docker kill container-id`
 - then to run a shell in the container: `docker exec -ti container-id bash`
