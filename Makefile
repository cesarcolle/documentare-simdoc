MVN=mvn ${MAVEN_CLI_OPTS} ${MAVEN_OPTS}
VERSION=`git describe`

# Used to build debian package
# here we consider that artifacts are already built (to speed up continuous integration)
# so we do nothing here
build:

# Build debian package
deb:
	dch -v ${VERSION} "git update, version ${VERSION}"
	bash .dh_build.sh
	mv ../simdoc*.deb .

# Debian package install
install:
	rm -rf usr && mkdir -p usr/share/java
	cp simdoc/clustering-server/target/clustering-server-*.jar usr/share/java/clustering-server.jar
	cp simdoc/mediation-server/target/mediation-server-*.jar usr/share/java/mediation-server.jar
	rm -rf etc && mkdir -p etc/init.d && cp debian/clustering-server-init-d etc/init.d/clustering-server
	cp debian/mediation-server-init-d etc/init.d/mediation-server
	mkdir etc/rc2.d && (cd etc/rc2.d && ln -s ../init.d/clustering-server S08clustering-server && ln -s ../init.d/mediation-server S08mediation-server )

clean:
	debclean


deploy-core-snapshots:
	(cd simdoc/core/java/ && ${MVN} clean deploy)

build-servers:
	(cd simdoc/clustering-server/ && ${MVN} install)
	(cd simdoc/mediation-server/ && ${MVN} install)

build-apps:
	(cd simdoc/apps && ${MVN} install)

integration-test:
	(cd simdoc/apps/ && ./refIntegrationTest.sh)

tar_apps:
	rm -rf apps && mkdir -p apps
	cp simdoc/apps/line-detection/target/line*.jar apps/line-detection.jar
	cp simdoc/apps/prep-data/target/prep*.jar apps/prep-data.jar
	cp simdoc/apps/ncd/target/ncd*.jar apps/ncd.jar
	cp simdoc/apps/ncd-remote/target/ncd*.jar apps/ncd-remote.jar
	cp simdoc/apps/clustering-remote/target/clustering-remote*.jar apps/clustering-remote.jar
	cp simdoc/apps/prep-clustering/target/prep*.jar apps/prep-clustering.jar
	cp simdoc/apps/similarity-clustering/target/sim*.jar apps/similarity-clustering.jar
	cp simdoc/apps/graph/target/graph*.jar apps/graph.jar
	tar cvjf apps.tar.bz2 apps


