<a name=""></a>
# [](//compare/1.70.0...vundefined) (2017-08-08)


### Bug Fixes

* **prep-data:** update cmd line tool usage f1aa112



<a name="1.70.0"></a>
# [1.70.0](//compare/1.60.0...1.70.0) (2017-08-08)


### Bug Fixes

* **gitlab-ci:** copy jar of clustering-server and mediation-server for docker image 69b0217
* **gitlab-ci:** copy jar of clustering-server and mediation-server for docker image 6ae5ff9
* **raw images:** convert to given pixels size, and not bytes size fb04dc7



<a name="1.60.0"></a>
# [1.60.0](//compare/1.51.0...1.60.0) (2017-08-07)




<a name="1.51.0"></a>
# [1.51.0](//compare/1.50.3...1.51.0) (2017-07-26)


### Features

* **clustering-remote:** add test whith mediation-server and clustering-server 4a2dd28
* **clustering-remote:** add url parameter for remote server url a367e8c
* **clustering-remote:** revert to use inputDirectory 0370efc
* **clustering-remote:** use clustering-server a30e484
* **clustering-server:** delete prep data 799ca1d
* **mediation-server:** delete unused code 076b8f6
* **mediation-server:** delete unused code ee9b5ec
* **mediation-server:** delete unused file and change parameter for access clustering server c0369a9
* **mediation-server:** first implem of mediation-server 217f5ec
* **mediation-server:** first implem of mediation-server a1453a9
* **mediation-server:** url to access clustering-server is a parameter d795097
* **servers:** add mediation-server e30ab71
* **servers:** rename simdoc-server 3fe7257



<a name="1.50.3"></a>
## [1.50.3](//compare/1.50.2...1.50.3) (2017-07-11)


### Bug Fixes

* **deb pkg:** add startup script and fix missing deps c4e7d4f



<a name="1.50.2"></a>
## [1.50.2](//compare/1.50.1...1.50.2) (2017-07-07)


### Bug Fixes

* **release:** tools jar are added to the delivery 57b659d



<a name="1.50.1"></a>
## [1.50.1](//compare/1.50.0...1.50.1) (2017-07-07)


### Bug Fixes

* **ci:** add jar tools to delivery 173ba49
* **deb:** set version to git version 85fbe62



<a name="1.50.0"></a>
# [1.50.0](//compare/1.44.0...1.50.0) (2017-07-07)


### Bug Fixes

* **clustering-remote:** cleanup ff290bf
* **clustering-remote:** test and run are ok 4247046
* **simdoc-server:** bytes data (filepath), remap from converted files d18f086

### Features

* **raw:** add rescaling d0b48c9



<a name="1.44.0"></a>
# [1.44.0](//compare/1.41.2...1.44.0) (2017-07-04)


### Bug Fixes

* **simdoc server:** bytes data mode with embeded bytes is ok 486be16
* **simdoc-server:** bytesData mode should be computed dynamically ca2541c

### Features

* **prep-data:** support bytes data for raw conversion 90de6b9
* **simdoc server:** add support for bytes data and raw mode 5febbab



<a name="1.41.2"></a>
## [1.41.2](//compare/1.41.1...1.41.2) (2017-06-13)


### Bug Fixes

* **server:** reduce test logs to avoid gitlab-ci issue 71419e6



<a name="1.41.1"></a>
## [1.41.1](//compare/1.41.0...1.41.1) (2017-06-13)


### Bug Fixes

* **server:** keep nearest items for singleton experimentation 384b2a8



<a name="1.41.0"></a>
# [1.41.0](//compare/1.40.2...1.41.0) (2017-06-13)


### Bug Fixes

* **ncd-remote:** add local machines instead of localhost 6dc212b

### Features

* **ncd-remote:** kill all remote tasks first 60522b8
* **ncd-remote:** new request just after a response e7d1aff
* **server:** add kill-all-tasks endpoint 3ff1b38
* **server:** reject request if max tasks nb is reached 470b37d



<a name="1.40.2"></a>
## [1.40.2](//compare/1.40.1...1.40.2) (2017-06-12)


### Bug Fixes

* **ncd-remote:** bytes data mode ok now e1c81e5

### Features

* **server:** add sloop 2a97239
* **server:** sloop + use tasks pattern for clustering 2e19000



<a name="1.40.1"></a>
## [1.40.1](//compare/1.40.0...1.40.1) (2017-06-08)




<a name="1.40.0"></a>
# [1.40.0](//compare/1.37.0...1.40.0) (2017-06-07)


### Bug Fixes

* **feign:** disable read timeout... 2d4ccbf
* **ncd:** compress cache use hashcode as key 84bb0c1

### Features

* **bytesdata:** lazy loading for files 761671f
* **BytesData:** add file cache a421dde
* **cache:** rely on guava cache for Ncd C(x) and BytesData file 47b4ef0
* **local dispatch:** WIP, need to add feign now 23bf82a
* **ncd remote:** import 064b45c
* **ncd-remote:** handle request error, with replay e24f65e
* **ncd-remote:** implement remote services management 31d177f
* **ncd-remote:** now wait for result 7c0caf6
* **ncd-remote:** WIP, build segments bd0b205
* **ncd-remote:** WIP, requestsExecutor b9073fb
* **nearest:** include nearest in prep clustering to test singletons graph rebuild 482e0b9
* **raspy:** add graphviz build 71ad52f
* **server:** add tasks b8af081
* **server:** get result through task api 8c31174
* **server distances:** WIP, test is green, should add animals it test fef389d
* **server distances:** WIP, test is red 184be55



<a name="1.37.0"></a>
# [1.37.0](//compare/1.36.0...1.37.0) (2017-04-11)


### Bug Fixes

* **clustering params:** sloop implies scut 0bdca05
* **graph:** significantly improve SCUT, it was buggy... 20ba34e
* **sloop:** do not work on subgraph matching the one cluster criteria 136c43e

### Features

* **clusteringParameters:** add sloop 6c59fb8
* **Graph:** first version of sloop implemented, ready for jojo play with e583b93
* **simclustering:** add sloop option bfc05fa
* **simclustering:** add sloop option, fix it test 26a768f



<a name="1.36.0"></a>
# [1.36.0](//compare/1.35.0...1.36.0) (2017-03-16)




<a name="1.35.0"></a>
# [1.35.0](//compare/1.34.0...1.35.0) (2017-03-13)


### Bug Fixes

* **thumbnails:** only display first page 3e0ae24

### Features

* **base64:** add base64 tool f1f3cb3
* **base64:** add base64 tool 04743c6
* **base64:** WIP first import 5a7b548
* **thumbnails:** reduce label size to 24pt 1e9b5d0



<a name="1.34.0"></a>
# [1.34.0](//compare/1.33.0...1.34.0) (2017-03-10)


### Features

* **thumbnails:** add label bd91775
* **thumbnails:** add possibility to use a different directory to build thumbnails e58dfe1
* **thumbnails:** create valid symlinks to use as thumbnail source image c82a4b8
* **thumbnails:** move to System module, and rely on convert b9fec66
* **thumbnails:** speedup thumbanails gen in graph f8fcf3a



<a name="1.33.0"></a>
# [1.33.0](//compare/1.32.0...1.33.0) (2017-03-07)


### Bug Fixes

* **matrix csv:** avoid locale issue (float number) by specifying Locale.US 3b90916

### Features

* **clustering:** #41 remove edges greater or equal to 1 5aca85f



<a name="1.32.0"></a>
# [1.32.0](//compare/1.31.0...1.32.0) (2017-02-17)


### Features

* **apps:** work with new prep-data 5ab28a8
* **BytesData:** add possibility to load bytes from BytesData with only a filepath 167f243
* **BytesData:** build without loading bytes, option 887b6de
* **BytesDistances:** add class to compute distances on BytesData objects 85fdf01
* **Graph:** graph can build its own thumbnails 560a2c9
* **ncd:** ncd can work on bytes data json files b6947a2
* **NCD:** add JoTOphe tag to all NCD inputs 0f67eef
* **NCD:** add JoTOphe tag to all NCD inputs 9899efa
* **NCD:** add JoTOphe tag to all NCD inputs 0570f5a
* **NCD:** uses BytesDistances now 57a46aa
* **prep-data:** always generate bytes-data 15445e5
* **PrepData:** add new PrepData module, in charge of prepping data up front ncd 6c4aa09
* **PrepData:** can embed bytes, add rawConversion to metadata json 555eafb
* **PrepData:** raw converter is available 14255b8
* **PrepData:** WIP raw converter to be implemented 66948c6
* **PrepData:** WIP raw converter to be implemented 15530a8
* **simdoc-server:** WIP add bytesData API 45e561d
* **simdocserver:** cleanup clustering service c650e7b
* **thumbnail:** create thumbnail with opencv, remove image magick dependency eb361b3
* **thumbnail:** WIP create thumbnail with opencv, remove image magick dependency a8a5655



<a name="1.31.0"></a>
# [1.31.0](//compare/1.30.0...1.31.0) (2017-01-13)


### Features

* **core:** skip hidden files bc11b50
* **core:** WIP skip hidden files 1d44b49
* **core:** WIP skip hidden files 12851ad



<a name="1.30.0"></a>
# [1.30.0](//compare/1.20.0...1.30.0) (2017-01-12)


### Bug Fixes

* **build:** fix build after dep name change... 4f5b5be
* **cmdline:** fix command line errors 6dfe737
* **Graph:** do not add image if image directory is not provided d0355a1
* **kNN:** orphan element is a singleton; use K+1 and not K 977483f
* **server:** clustering result: filename relative to input directory f040ea4
* **server:** fix validation when using shared directory bb2f6bc
* **server:** safe input dir must be in output dir 6f6d951
* **SimClustering:** use commons-cli snapshot since optional opt issue is fixed 97dacd8
* **thumbnail:** page selector needs to be escaped... 4dfddaa

### Features

* **clustering:** acut & qcut are optional 67e5e96
* **FilesIO:** add files id builder 66c52de
* **Graph:** support inital simple filename as escaped html label 2578a59
* **KNN:** add knn option to PrepClustering 45b0337
* **KNN:** add knn option to PrepClustering 4f12b47
* **KNN:** introduce KNN feature in graph build fc39dfc
* **KNN:** introduce KNN feature in TriangleVertices 6c4a78e
* **KNN:** PrepClustering, make it build before introducing KNN fe06fe4
* **NCD:** update command line options 996bd55
* **PrepInputDir:** add PrepInputDir 51fc420
* **PrepInputDir:** WIP init PrepInputDir 9e69ce3
* **server:** implement clustering request validation; invalid request tests written 02d5353
* **server:** improve API doc 7b097ef
* **server:** improve clustering request error reporting to client 5ba8cd4
* **server:** introduce shared directory ce9d457
* **server:** purge output dir before working 6eb0424
* **server:** remove leading '/' from relative filename eb8c528
* **server:** save request parameters on disk a1bec44
* **SimClustering:** lost in optinal parameters hell c9725d8
* **SimClustering:** start revamping parameters d14ba0a
* **SimdocServer:** import first empty shell, whaou banner :) c529071
* **swagger:** enable swagger ui to document REST API a48f23f
* **thumbnail:** support bmp files 05c0fb9
* **Thumbnails:** is a separate module, for the sake of SRP concern 9106461



<a name="1.20.0"></a>
# [1.20.0](//compare/0.0.2...1.20.0) (2016-11-23)




<a name="0.0.1"></a>
## [0.0.1](//compare/a3c394b...0.0.1) (2016-11-23)


### Features

* **Simdoc:** import from internal gitlab branch 34-prep-open-source-export-to-github, commit  a3c394b



