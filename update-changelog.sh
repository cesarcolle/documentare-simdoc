#!/bin/sh

# https://www.npmjs.com/package/conventional-changelog-cli
conventional-changelog -p angular -i CHANGELOG.md -w -r 0
